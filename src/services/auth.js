import { apiAuth, apiNoAuth } from "../config/axios";
export const signUp = async (userData) => {
  try {
    const { data } = await apiNoAuth.post("/sign-up", userData);
    return data;
  } catch (error) {
    throw error;
  }
};
export const signIn = async (credentials) => {
  try {
    const { data } = await apiNoAuth.post("/sign-in", credentials);
    if (data.accessToken && data.accessToken.split(".").length === 3) {
      localStorage.setItem("authToken", data.accessToken);
    } else {
      console.error("Invalid token format, not saving to localStorage");
    }
    if (data.customer) {
      const { full_name, birthday, phone, email, gender } = data.customer;
      const customerData = {
        full_name,
        birthday,
        phone,
        email,
        gender,
      };
      localStorage.setItem("customer", JSON.stringify(customerData));
    }
    return data;
  } catch (error) {
    throw error;
  }
};

export const changePassword = async (data) => {
  try {
    const { res } = await apiAuth.post("/change-password", data);
    return res;
  } catch (error) {
    throw error;
  }
};

export const forgotPassword = async (data) => {
  try {
    const { res } = await apiNoAuth.post("/forgot-password", data);
    return res;
  } catch (error) {
    throw error;
  }
};

export const logout = async () => {
  try {
    const { data } = await apiNoAuth.post("/logout");
    localStorage.removeItem("authToken");
    localStorage.removeItem("customer");
    return data;
  } catch (error) {
    throw error;
  }
};
