import { apiNoAuth } from "../config/axios";

export const get = async () => {
  try {
    const { data } = await apiNoAuth.get(`/movies`);
    return data;
  } catch (error) {
    throw error;
  }
};
export const getById = async (id) => {
  try {
    const { data } = await apiNoAuth.get(`/movies/${id}`);
    return data;
  } catch (error) {
    throw error;
  }
};
