import { apiNoAuth } from "../config/axios";

export const getSchedulesByMovieId = async (id) => {
  try {
    const { data } = await apiNoAuth.get(`/movie/${id}/schedules`);
    return data;
  } catch (error) {
    throw error;
  }
};
