import axios from "axios";
import { apiAuth } from "../config/axios";

export const processTransaction = async (paymentData) => {
  try {
    const response = await axios.post(
      `https://server-qfjq.onrender.com/api/v1/payment/${paymentData.payment_method}`,
      paymentData
    );
    return response;
  } catch (error) {
    throw error;
  }
};
