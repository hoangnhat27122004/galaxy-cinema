import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import BasicLayout from "../layout/BasicLayout";
import Account from "../pages/Account/Account";
import SignIn from "../pages/Auth/SignIn";
import SignUp from "../pages/Auth/SignUp";
import Booking from "../pages/Booking/Booking";
import Home from "../pages/Home/Home";
import MediaPage from "../pages/Media/MediaPage";
import MovieDetail from "../pages/Movie/Detail";

const RouterComponent = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<BasicLayout />}>
          <Route index element={<Home />} />
          <Route path="sign-in" element={<SignIn />} />
          <Route path="sign-up" element={<SignUp />} />
          <Route path="dat-ve/:id" element={<MovieDetail />} />
          {/* <Route path="booking" element={<Booking />} /> */}
          <Route path="booking/:id" element={<Booking />} />
          <Route path="movie/:id" element={<MediaPage />} />
          <Route path="tai-khoan/" element={<Account />} />
        </Route>
      </Routes>
    </Router>
  );
};

export default RouterComponent;
