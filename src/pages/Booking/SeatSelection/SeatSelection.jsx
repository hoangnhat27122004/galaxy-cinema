import { useEffect, useState } from "react";
import Dialog from "../../../components/base/Dialog/Dialog";

const SeatSelection = ({
  data,
  selectedSeats,
  scheduleData,
  onSelectedSeatsChange,
  onScheduleChange,
}) => {
  const [dataSeat, setDataSeat] = useState([]);
  const [currentSchedule, setCurrentSchedule] = useState(data);
  const [showDialog, setShowDialog] = useState(false);

  useEffect(() => {
    if (currentSchedule) {
      setDataSeat(currentSchedule.seats?.map((s) => s) || []);
    }
  }, [currentSchedule]);

  useEffect(() => {
    if (data) {
      setCurrentSchedule(data);
    }
  }, [data]);

  const groupedSeats = dataSeat.reduce((acc, seat) => {
    if (!acc[seat.name]) {
      acc[seat.name] = [];
    }
    acc[seat.name].push(seat);
    return acc;
  }, {});

  const sortedGroupedSeats = Object.keys(groupedSeats)
    .sort()
    .reduce((acc, key) => {
      acc[key] = groupedSeats[key].sort((a, b) => a.column - b.column);
      return acc;
    }, {});

  const handleSeatSelection = (seat) => {
    let newSelectedSeats;
    if (selectedSeats.some((s) => s.id === seat.id)) {
      newSelectedSeats = selectedSeats.filter((s) => s.id !== seat.id);
    } else if (selectedSeats.length >= 8) {
      setShowDialog(true);
      return;
    } else {
      newSelectedSeats = [...selectedSeats, seat];
    }
    onSelectedSeatsChange(newSelectedSeats);
  };

  const onHandleScheduleChange = (schedule) => {
    setCurrentSchedule(schedule);
    onScheduleChange(schedule);
  };

  const maxRowLength = Math.max(
    ...Object.values(groupedSeats).map((seats) => seats.length)
  );

  return (
    <div>
      {showDialog && (
        <Dialog
          message={"Quý khách không được chọn quá 8 ghế trong 1 lần đặt vé."}
          onCancel={() => setShowDialog(false)}
        />
      )}
      <div className="bg-white py-4 rounded md:mb-8 mb-4 w-[100%]">
        <div className="grid md:grid-cols-10 grid-cols-2">
          <div className="md:col-span-2">
            <label className="md:text-base text-sm font-semibold inline-block mt-2">
              Đổi suất chiếu
            </label>
          </div>
          <div className="col-span-8 flex-row gap-4 flex-wrap items-center md:flex hidden">
            {scheduleData?.map((schedule) => (
              <button
                key={schedule.id}
                onClick={() => onHandleScheduleChange(schedule)}
                className={`py-2 px-4 border rounded text-sm font-normal animate-fade duration-500 ease-in-out hover:bg-sky-700 hover:text-white
                ${
                  currentSchedule?.id === schedule.id
                    ? "bg-sky-700 text-white"
                    : ""
                }
                `}
              >
                {schedule.start_time.slice(0, 5)}
              </button>
            ))}
          </div>
        </div>
      </div>
      <div>
        {Object.keys(sortedGroupedSeats).map((group) => (
          <div
            key={group}
            className="mb-4 flex items-center justify-between animate-fade"
          >
            <h3>{group}</h3>
            <div
              style={{
                gridTemplateColumns: `repeat(${maxRowLength}, 1fr)`,
              }}
              className="grid gap-2"
            >
              {sortedGroupedSeats[group]?.map((seat) => (
                <div key={seat.id} className="relative">
                  <button
                    className={`animate-fade-up  md:h-6 h-5 border rounded md:text-s text-[10px] transition duration-200 ease-in-out md:w-6 w-5 border-grey-20
                      ${
                        selectedSeats.some((s) => s.id === seat.id) &&
                        "bg-orange-500 text-white"
                      }
                      ${
                        seat.schedule_seat.status !== "AVAILABLE"
                          ? "bg-slate-500 - text-white cursor-not-allowed"
                          : "hover:text-white xl:hover:bg-orange-500 xl:hover:border-orange-500"
                      }
                      `}
                    onClick={() => handleSeatSelection(seat)}
                    disabled={seat.schedule_seat.status !== "AVAILABLE"}
                  >
                    {seat.column}
                  </button>
                </div>
              ))}
            </div>
            <h3>{group}</h3>
          </div>
        ))}

        <div className="flex gap-4">
          <div className="flex gap-3">
            <div className="rounded md:w-6 w-5 md:h-6 h-5 bg-slate-500 "></div>
            <p>Ghế đã đặt</p>
          </div>
          <div className="flex gap-3">
            <div className="rounded md:w-6 w-5 md:h-6 h-5 bg-orange-500 "></div>
            <p>Ghế đang chọn </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SeatSelection;
