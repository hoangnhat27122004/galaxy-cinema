import { joiResolver } from "@hookform/resolvers/joi";
import React from "react";
import { useForm } from "react-hook-form";
import { bookingWithoutLoginValidator } from "../../validations/Auth";
import FormField from "../../components/UI/FormField";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useNavigate } from "react-router-dom";

const BookingWithoutLogin = ({ onClose }) => {
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    control,
  } = useForm({
    resolver: joiResolver(bookingWithoutLoginValidator),
  });
  const onSubmit = (data) => {
   navigate('/booking/meo-beo-sieu-quay-1')
  };

  return (
    <React.Fragment>
      <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-50 transition-all bg-black bg-opacity-45">
        <div
        //   ref={ref}
        >
          <div className="bg-white max-w-[400px] ssm:min-w-[360px] min-w-[400px] rounded-md p-8 relative m-2">
            <button
              onClick={onClose}
              className="absolute left-5 cursor-pointer hover:underline"
            >
              <FontAwesomeIcon icon={faChevronLeft} /> Quay lại
            </button>
            <img
              src="https://www.galaxycine.vn/_next/static/media/bear_glx.d5131c11.png"
              alt=""
              className="mx-auto w-40 h-36"
            />
            <p className="capitalize font-medium text-lg text-center my-2">
              Nhập thông tin
            </p>
            <div className="container mx-auto">
              <div className="max-w-md mx-auto mt-10">
                <form onSubmit={handleSubmit(onSubmit)}>
                  <div className="mb-6">
                    <FormField
                      label="Số điện thoại"
                      type="numeric"
                      placeholder="Nhập số điện thoại của bạn"
                      error={errors.phone}
                      {...register("phone")}
                    />
                  </div>
                  <div className="mb-6">
                    <FormField
                      label="Email"
                      type="email"
                      placeholder="you@gmail.com (net)"
                      error={errors.email}
                      {...register("email")}
                    />
                  </div>

                  <div className="">
                    <button
                      type="submit"
                      className="capitalize w-full px-3 py-4 text-white bg-indigo-500 rounded-md focus:bg-indigo-600 focus:outline-none"
                    >
                      Tiếp tục
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default BookingWithoutLogin;
