import { faAngleDown, faAngleUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { paymentMethods } from "./PaymentStructure";
import Dialog from "../../../components/base/Dialog/Dialog";

const PaymentMethodsSelection = ({ onPaymentMethod }) => {
  const [stars, setStar] = useState();
  const [selectedMethod, setSelectedMethod] = useState("ZaloPay");
  const [showDialog, setShowDialog] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const toggleCollapse = () => {
    setIsOpen(!isOpen);
  };
  const handlePaymentMethodChange = (methodValue) => {
    if (methodValue !== "ZaloPay") {
      setShowDialog(true);
    } else {
      setSelectedMethod(methodValue);
      onPaymentMethod(methodValue);
    }
  };
  const handleDialogCancel = () => {
    setShowDialog(false);
    setSelectedMethod("ZaloPay");
    onPaymentMethod("ZaloPay");
  };
  return (
    <>
      {showDialog && (
        <Dialog
          message={"Hiện tại bên chúng tôi mới hỗ trợ thanh toán với ZaloPay"}
          onCancel={handleDialogCancel}
        />
      )}
      <div className="bg-white p-4">
        <h3 className="text-lg mb-4 font-semibold">Khuyến mãi</h3>
        <div className="md:mt-4 mt-2">
          <form action="">
            <div className="mt-4 grid grid-cols-2 gap-4 xl:w-2/3 w-full">
              <div className="col-span-1">
                <label
                  htmlFor=""
                  className="inline-block mb-1 text-black-10 text-sm font-bold"
                >
                  Mã khuyến mãi
                </label>
                <input
                  type="text"
                  name="barcode"
                  className="border w-full px-4 py-2"
                />
              </div>
              <div className="col-span-1 flex items-start mt-7">
                <button className="bg-orange-500 text-white text-sm rounded px-12 py-2 h-[42px]">
                  Áp dụng
                </button>
              </div>
            </div>
            <p className="text-s text-grey-40 mt-2">
              Lưu ý: Có thể áp dụng nhiều vouchers vào 1 lần thanh toán
            </p>
          </form>
          <div className="md:mt-4 mt-2 ">
            <div className="xl:w-2/3 w-full flex justify-between items-center cursor-pointer gap-4">
              <h4 className="inline-block mb-4 text-black-10 text-sm font-bold cursor-pointer flex-1">
                Khuyến mãi của bạn
                <span className="transition-all duration-300 ease-in-out ">
                  <FontAwesomeIcon
                    icon={faAngleDown}
                    style={{ color: "#000000" }}
                  />
                </span>
              </h4>
            </div>
          </div>
        </div>
        <>
          <div className="md:mt-4 mt-2">
            <div className="xl:w-2/3 w-full flex justify-between items-center cursor-pointer ap-4">
              <h4 className="inline-block mb-1 text-black-10 text-sm font-bold flex-1">
                Áp dụng điểm Stars{" "}
                <span className="transition-all duration-300 ease-in-out ">
                  <FontAwesomeIcon
                    className="transition-all duration-300 ease-in-out"
                    icon={isOpen ? faAngleUp : faAngleDown}
                    onClick={toggleCollapse}
                    style={{ color: "#000000" }}
                  />
                </span>
              </h4>
            </div>
            <div className="min-h-0 h-auto transition-all">
              {isOpen && (
                <div className="min-h-0 h-auto transition-all">
                  <div className="MuiCollapse-wrapper MuiCollapse-vertical css-hboir5">
                    <div className="MuiCollapse-wrapperInner MuiCollapse-vertical css-8atqhb">
                      <form autoComplete="off">
                        <div className="mt-4 grid grid-cols-2 gap-4 md:w-2/3 w-full">
                          <div className="col-span-1">
                            <input
                              id="star-point"
                              type="text"
                              className="border w-full py-2 px-4"
                              name="point"
                              defaultValue=""
                            />
                          </div>
                          <div className="col-span-1 flex items-start">
                            <button className="bg-orange-500 text-white text-sm rounded px-12 py-2  h-[42px]">
                              Áp Dụng
                            </button>
                          </div>
                        </div>
                        <p className="text-s text-grey-40 mt-2">
                          Bạn đang có {stars ? stars : 0} điểm Stars
                        </p>
                        <div className="mt-2 text-sm">
                          <h3>Lưu ý</h3>
                          <p>
                            Điểm Stars có thể quy đổi thành tiền để mua vé hoặc
                            bắp/nước tại các cụm rạp Galaxy Cinema.
                          </p>
                          <ul>
                            <li>1 Stars = 1,000 VNĐ</li>
                            <li>
                              Stars quy định trên 1 giao dịch: tối thiểu là 20
                              điểm và tối đa là 100 điểm.
                            </li>
                          </ul>
                          <p>
                            Stars là điểm tích lũy dựa trên giá trị giao dịch
                            bởi thành viên giao dịch tại Galaxy Cinema. Cơ chế
                            tích lũy stars, như sau:
                          </p>
                          <ul>
                            <li>
                              Thành viên Star: 3% trên tổng giá trị/ số tiền
                              giao dịch.
                            </li>
                            <li>
                              Thành viên G-Star: 5% trên tổng giá trị/ số tiền
                              giao dịch.
                            </li>
                            <li>
                              Thành viên X-Star: 10% trên tổng giá trị/ số tiền
                              giao dịch.
                            </li>
                          </ul>
                          <p>
                            Ví dụ: hóa đơn giao dịch của khách hàng là 100,000
                            VNĐ, hạng thành viên Star. Số stars tích được sẽ là
                            5 stars, tương đương với 5,000 VNĐ.
                          </p>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </>
      </div>
      <div className="bg-white p-4 mt-8">
        <h3 className="text-l mb-4 font-semibold">Phương thức thanh toán</h3>
        <div className="my-4">
          <div className="leading-5 text-l">
            {paymentMethods.map((method) => (
              <li className="mb-4 md:block flex items-center" key={method.id}>
                <button
                  className="mb-4 md:block flex items-center cursor-pointer w-full text-left"
                  onClick={() => handlePaymentMethodChange(method.value)}
                >
                  <input
                    type="radio"
                    className="w-4 h-4 text-primary bg-gray-100 border-gray-300"
                    name="payment-methods"
                    id={method.id}
                    checked={selectedMethod === method.value}
                    onChange={() => {}}
                  />
                  <img
                    alt={method.value}
                    loading="lazy"
                    className="w-[50px] h-[50px] inline-block mx-2 object-cover duration-500 ease-in-out group-hover:opacity-100 scale-100 blur-0 grayscale-0"
                    src={method.src}
                    style={{ color: "transparent" }}
                  />
                  <label
                    htmlFor={method.id}
                    className="text-base cursor-pointer"
                  >
                    {method.label}
                  </label>
                </button>
              </li>
            ))}
          </div>
        </div>
        <div className="mt-8 text-sm">
          <strong className="text-red-10 font-semibold">(*) </strong>
          <span>
            Bằng việc click/chạm vào THANH TOÁN bên phải, bạn đã xác nhận hiểu
            rõ các Quy Định Giao Dịch Trực Tuyến của {"Tên App"}.
          </span>
        </div>
      </div>
    </>
  );
};

export default PaymentMethodsSelection;
