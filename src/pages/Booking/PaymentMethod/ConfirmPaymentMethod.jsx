import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faX } from "@fortawesome/free-solid-svg-icons";
import { processTransaction } from "../../../services/payment";
import {
  dayAndMonth,
  formatDate,
  formatSchedule,
  formatScheduleBooking,
} from "../../../systems/ultis/FormatDate";
const ConfirmPaymentMethod = ({ data, onClose, paymentMethod }) => {
  const totalSeatPrices = data.seat.reduce(
    (total, seat) => total + seat.seat_type.price,
    0
  );
  const numberOfSeats = data.seat.length;
  const moviePrice = data.movie.price;

  const total = numberOfSeats * moviePrice + totalSeatPrices;
  const [isChecked, setIsChecked] = useState(false);
  const user = JSON.parse(localStorage.getItem("customer"));
  const handlePayment = async () => {
    const paymentData = {
      payment_method: paymentMethod.toLowerCase(),
      total: total,
      schedule_id: data.schedule.id,
      seat_id: data.seat.map((s) => s.id),
      phone: user?.phone,
      email: user?.email,
    };
    try {
      const result = await processTransaction(paymentData);
      if (result.data.order_url) {
        window.location.href = result.data.order_url;
      } else {
        console.log("Kết quả thanh toán:", result);
      }
    } catch (error) {
      console.error("Lỗi khi xử lý thanh toán:", error.message);
    }
  };
  return (
    <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-50 transition-all">
      <div className="bg-black bg-opacity-50 fixed inset-0 z-[-1]"></div>
      <div>
        <div className="flex justify-center">
          <button
            onClick={onClose}
            className="border-0 p-0 cursor-pointer bg-transparent pb-4"
          >
            <FontAwesomeIcon
              icon={faX}
              className="w-[25px] h-[25px] rounded-full outline-none"
              style={{ color: "#f7f7f7" }}
            />
          </button>
        </div>
        <div className="py-5 px-6 rounded-lg bg-white">
          <h1 className="uppercase text-[16px] font-bold text-center">
            Thông tin đặt vé
          </h1>
          <div className=" mb-5 text-center">
            Phương thức thanh toán {paymentMethod}
          </div>
          <div>
            <div className="flex gap-x-4 items-start">
              <p className="text-[14px] font-bold flex-none w-[30px] py-4">
                Phim
              </p>
              <div className="border border-[#DCD8D8] flex-1 text-left p-4">
                <p className="text-[16px] font-bold text-[#1353B4]">
                  {data.movie.name}
                </p>
                <p className="text-[14px] ">
                  <span>Độ tuổi yêu cầu:</span>
                  <span className="ml-2 bg-orange-400 rounded-sm">
                    <span className="inline-flex items-center justify-center  px-2 bg-primary  text-sm text-center text-white font-bold not-italic">
                      16
                    </span>
                  </span>
                </p>
              </div>
            </div>
            <div>
              <div className="flex gap-x-4 my-3 items-start">
                <p className="text-[14px] font-bold flex-none w-[30px] py-4">
                  Phòng
                </p>
                <div className="border border-[#DCD8D8] flex-1 text-left p-4">
                  <p className="text-[16px] font-bold text-[#1353B4]">
                    {data.schedule.room.name}
                  </p>
                  {/* formatSchedule(schedule.date)}</span>
                      <span>{dayAndMonth(schedule.date) */}
                  <p className="text-[14px] font-bold capitalize">
                    {data.schedule.start_time.slice(0, 5)} -{" "}
                    {formatScheduleBooking(data.schedule.show_date)},
                    <strong className="ml-1">
                      {formatDate(data.schedule.show_date)}
                    </strong>
                  </p>
                </div>
              </div>
              <div className="flex gap-x-4 my-3 items-center">
                <div className="text-[14px] font-bold flex-none w-[30px]">
                  Ghế
                </div>
                <div className="border border-[#DCD8D8] flex-1 text-left p-4">
                  {/* <p className="text-[14px]">RAP 5</p> */}
                  <div className="flex justify-between text-[14px]">
                    <div>
                      <span>
                        {data.seat
                          .map((seat) => `${seat.name}${seat.column}`)
                          .join(", ")}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex gap-x-4 my-5 items-center">
            <p className="text-[14px] font-bold flex-none w-[30px]">Tổng</p>
            <div className="flex-1 text-left px-4 py-2 bg-[#1353B4] text-white font-bold">
              <p>{total.toLocaleString("vi-VN")} VNĐ</p>
            </div>
          </div>
          <span className="w-full border-b-2 border-dashed h-[2px] inline-block border-gray-300"></span>
          <div className="flex items-center mt-5">
            <img
              className="w-[36px] h-[40px] flex-none"
              src="https://www.galaxycine.vn/_next/static/media/icon-success.c42e4e43.png"
              alt=""
            />
            <p className="min-[412px]:text-[14px] text-[12px] flex-auto ml-1">
              <i>Tôi xác nhận các thông tin đặt vé đã chính xác</i>
            </p>
            <input
              type="checkbox"
              className="ml-2 hover:cursor-pointer"
              onChange={(e) => setIsChecked(e.target.checked)}
            />
          </div>

          <div className="text-center mt-2">
            <button
              aria-label="Vui lòng chọn xác nhận thông tin đặt vé"
              className={`w-1/2 py-2 text-white border rounded-md mx-auto font-bold ${
                isChecked
                  ? "bg-orange-500 hover:bg-orange-600"
                  : "bg-gray-300 cursor-not-allowed"
              }`}
              onClick={handlePayment}
              disabled={!isChecked}
            >
              Xác Nhận Thanh Toán
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ConfirmPaymentMethod;
