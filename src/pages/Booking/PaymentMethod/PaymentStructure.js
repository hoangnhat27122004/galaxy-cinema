export const paymentMethods = [
  {
    id: "payment-with-hsbc",
    value: "HSBC",
    src: "https://cdn.galaxycine.vn/media/2020/10/20/hsbc-icon_1603203578522.png",
    label: "HSBC/Payoo - ATM/VISA/MASTER/JCB/QRCODE",
  },
  {
    id: "payment-with-shopeepay",
    value: "ShopeePay",
    src: "https://cdn.galaxycine.vn/media/2022/4/29/shopee-pay_1651229746140.png",
    label: "Ví ShopeePay - Nhập mã: SPPCINE07 Giảm 10K cho đơn từ 100K",
  },
  {
    id: "payment-with-momo",
    value: "MoMo",
    src: "https://cdn.galaxycine.vn/media/2020/10/20/momo-icon_1603203874499.png",
    label: "Ví Điện Tử MoMo",
  },
  {
    id: "payment-with-zalopay",
    value: "ZaloPay",
    src: "https://cdn.galaxycine.vn/media/2022/12/2/icon-96x96_1669977824597.png",
    label: "ZaloPay - Bạn mới Zalopay nhập mã GLX50 - Giảm 50k cho đơn từ 200k",
  },
  {
    id: "payment-with-vnpay",
    value: "VNPAY",
    src: "https://cdn.galaxycine.vn/media/2021/12/2/download_1638460623615.png",
    label: "VNPAY",
  },
];
