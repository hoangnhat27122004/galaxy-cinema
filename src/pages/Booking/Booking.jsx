import { useEffect, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import useScheduleQuery from "../../hooks/Schedule/useScheduleQuery";
import {
  formatDate,
  formatScheduleBooking,
} from "../../systems/ultis/FormatDate";
import DrinkSelection from "./DrinkSelection/DrinkSelection";
import ConfirmPaymentMethod from "./PaymentMethod/ConfirmPaymentMethod";
import PaymentMethodsSelection from "./PaymentMethod/PaymentMethodsSelection";
import SeatSelection from "./SeatSelection/SeatSelection";

const Booking = () => {
  const { id } = useParams();
  const [bookingData, setBookingData] = useState(null);
  const { data: allSchedulesData } = useScheduleQuery(id);
  const navigate = useNavigate();
  const [active, setActive] = useState(0);
  const [paymentMethod, setPaymentMethod] = useState("ZaloPay");
  const [showPopup, setShowPopup] = useState(false);
  const steps = ["Chọn ghế", "Chọn đồ ăn", "Thanh toán", "Xác nhận"];

  const handleNextStep = () => {
    if (active < steps.length - 2) {
      setActive(active + 1);
    } else if (active === steps.length - 2) {
      setShowPopup(true);
    }
  };

  const handleClosePopup = () => {
    setShowPopup(false);
  };

  const handlePaymentMethodSelect = (method) => {
    setPaymentMethod(method);
  };

  useEffect(() => {
    const storedBookingData = localStorage.getItem("quote");
    if (storedBookingData) {
      setBookingData(JSON.parse(storedBookingData));
    }
  }, []);

  useEffect(() => {
    if (allSchedulesData) {
      if (allSchedulesData.message) {
        navigate("/");
        return;
      }

      if (bookingData) {
        const { selectedSchedule, schedulesForDate } = bookingData;

        if (!Array.isArray(allSchedulesData.schedules)) {
          navigate("/");
          return;
        }

        const selectedScheduleExists = allSchedulesData.schedules.some(
          (schedule) => schedule.id === selectedSchedule.id
        );

        const allSchedulesExist = schedulesForDate.every((scheduleForDate) =>
          allSchedulesData.schedules.some(
            (schedule) => schedule.id === scheduleForDate.id
          )
        );

        if (!selectedScheduleExists || !allSchedulesExist) {
          navigate("/");
        }
      }
    }
  }, [allSchedulesData, bookingData, navigate]);

  const updateBookingData = (newData) => {
    const updatedBookingData = {
      ...bookingData,
      ...newData,
      selectedSeats: [],
    };
    setBookingData(updatedBookingData);
    localStorage.setItem("quote", JSON.stringify(updatedBookingData));
  };

  const handleSelectedSeatsChange = (seats) => {
    if (bookingData) {
      const updatedBookingData = {
        ...bookingData,
        selectedSeats: seats,
      };
      setBookingData(updatedBookingData);
      localStorage.setItem("quote", JSON.stringify(updatedBookingData));
    }
  };

  const calculateTotalPrice = () => {
    if (bookingData && bookingData.selectedSeats && bookingData.movie) {
      const basePrice = bookingData.movie.price || 0;
      return bookingData.selectedSeats.reduce((total, seat) => {
        const seatTypePrice = seat.seat_type?.price || 0;
        return total + basePrice + seatTypePrice;
      }, 0);
    }
    return 0;
  };

  const renderContent = () => {
    if (!bookingData) {
      return null;
    }
    switch (active) {
      case 0:
        return (
          <SeatSelection
            data={bookingData.selectedSchedule}
            selectedSeats={bookingData.selectedSeats}
            scheduleData={bookingData.schedulesForDate}
            onSelectedSeatsChange={handleSelectedSeatsChange}
            onScheduleChange={(newSchedule) =>
              updateBookingData({ selectedSchedule: newSchedule })
            }
          />
        );
      case 1:
        return <DrinkSelection />;
      case 2:
        return (
          <PaymentMethodsSelection
            onPaymentMethod={handlePaymentMethodSelect}
          />
        );
      default:
        return null;
    }
  };

  return (
    <div>
      <div>
        <ul className="flex justify-center items-center md:text-base text-[12px] font-semibold w-full flex-nowrap text-gray-400">
          {steps.map((step, index) => (
            <li key={index} className="pt-4 mb-4 pl-0">
              <button
                className={`md:mx-3 mx-1 ${
                  index < active
                    ? "text-sky-700 opacity-55 cursor-not-allowed"
                    : index <= active
                    ? "text-sky-700"
                    : "opacity-50 cursor-not-allowed"
                }`}
                onClick={() => index <= active && setActive(index)}
                disabled={index > active || index < active}
              >
                {step}
              </button>
              <div
                className={`relative mt-4 h-[2px] before:inline-block before:w-full before:absolute before:left-0 before:h-[2px] before:bg-gray-300 ${
                  index <= active
                    ? "after:inline-block after:absolute after:left-0 after:h-[2px] after:bg-sky-700 after:w-full"
                    : ""
                }`}
              />
            </li>
          ))}
        </ul>
      </div>
      <div className="md:container md:mx-auto lg:max-w-[1200px] md:max-w-4xl md:px-0 sm:px-[45px] grid xl:grid-cols-3 grid-cols-1">
        <div className="col-span-2 xl:order-first order-last xl:h-full h-[full] overflow-hidden xl:overflow-auto lg:pb-3 xl:pb-32">
          <div className="px-6 w-full h-full">
            {bookingData ? (
              <div>{renderContent()}</div>
            ) : (
              <p className="flex justify-center items-center h-full">
                Không có dữ liệu đặt vé. Vui lòng chọn phim và lịch chiếu trước.
              </p>
            )}
          </div>
        </div>
        <div className="col-span-1 xl:pl-4 xl:order-none order-first py-4">
          <div className="md:mb-4">
            <div className="h-[6px] bg-orange-500 rounded-t-lg" />
            <div className="bg-white p-4 grid grid-cols-3 xl:gap-2 items-center">
              <>
                <div className="row-span-2 md:row-span-1 xl:row-span-2 mr-4">
                  <img
                    alt={bookingData?.movie?.name}
                    width={100}
                    height={150}
                    className="xl:w-full xl:h-full md:w-[180px]   hidden xl:block md:h-[220px] w-[90px] h-[110px] rounded object-cover duration-500 ease-in-out group-hover:opacity-100 scale-100 blur-0 grayscale-0"
                    style={{ color: "transparent" }}
                    src={
                      bookingData?.movie
                        ? bookingData?.movie.image_portrait.url
                        : "https://www.galaxycine.vn/_next/static/media/img-blank.bb695736.svg"
                    }
                  />
                  <img
                    alt={bookingData?.movie?.name}
                    className="xl:w-full xl:h-full lg:w-[120px]  xl:hidden block lg:h-[80px] rounded object-cover duration-500 ease-in-out group-hover:opacity-100 scale-100 blur-0 grayscale-0"
                    style={{ color: "transparent" }}
                    src={
                      bookingData?.movie
                        ? bookingData?.movie.image_landscape.url
                        : "https://www.galaxycine.vn/_next/static/media/img-blank.bb695736.svg"
                    }
                  />
                </div>
                <div className="flex-1 col-span-1 lg:col-span-1 row-span-1 xl:col-span-2">
                  <h3 className="text-sm xl:text-base font-bold xl:mb-2">
                    {bookingData?.movie?.name}
                  </h3>
                  <p className="text-sm inline-block">
                    {bookingData?.movie?.format}
                  </p>
                  <span> - </span>
                  {bookingData?.movie && (
                    <div className="xl:mt-2 ml-2 xl:ml-0 inline-block">
                      <span className="inline-flex items-center justify-center w-[38px] h-7 bg-orange-500 rounded text-sm text-center text-white font-bold not-italic">
                        {bookingData?.movie?.age_restriction}T
                      </span>
                    </div>
                  )}
                </div>
                <div className="flex-1 col-span-2 lg:col-span-1 xl:col-span-3">
                  <div>
                    <div className="xl:mt-4 text-sm xl:text-base">
                      Phòng: &nbsp;
                      <strong className="text-sm xl:text-base">
                        {bookingData?.selectedSchedule?.room?.name}
                      </strong>
                    </div>
                    <div className="xl:mt-2 text-sm xl:text-base">
                      <span>Suất: </span>
                      <strong>
                        {bookingData?.selectedSchedule?.start_time.slice(0, 5)}
                      </strong>
                      <span> - </span>
                      <span className="capitalize text-sm">
                        {formatScheduleBooking(
                          bookingData?.selectedSchedule?.show_date
                        )}
                        , &nbsp;
                        <strong>
                          {formatDate(bookingData?.selectedSchedule?.show_date)}
                        </strong>
                      </span>
                    </div>
                    {bookingData?.selectedSeats?.length > 0 && (
                      <div className="xl:mt-2 text-sm xl:text-base animate-fade-down">
                        <span>Ghế đã chọn: </span>
                        <strong>
                          {bookingData.selectedSeats
                            .map((seat) => `${seat.name}${seat.column}`)
                            .join(", ")}
                        </strong>
                      </div>
                    )}
                  </div>
                  <div className="my-4 border-t border-grey-60 border-dashed xl:block hidden" />
                </div>
                <div className="xl:flex hidden justify-between col-span-3">
                  <strong className="text-base">Tổng cộng</strong>
                  <span className="inline-block font-bold text-primary ">
                    {calculateTotalPrice().toLocaleString()}
                    &nbsp;₫
                  </span>
                </div>
              </>
            </div>
            <div className="mt-8 xl:flex hidden">
              <button
                onClick={() => active > 0 && setActive(active - 1)}
                className="w-1/2 mr-2 py-2 hover:text-orange-500"
              >
                <span>Quay lại</span>
              </button>
              <button
                disabled={bookingData?.selectedSeats?.length > 0 ? false : true}
                onClick={handleNextStep}
                className={`
                  ${
                    bookingData?.selectedSeats?.length > 0
                      ? "bg-orange-500 hover:bg-orange-400 animate-fade"
                      : " cursor-not-allowed bg-gray-500 animate-fade"
                  }
                  w-1/2 ml-2 py-2  text-white border rounded-md `}
              >
                <span>
                  {active === steps.length - 2 ? "Thanh toán" : "Tiếp tục"}
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="md:container md:mx-auto lg:max-w-[1200px] md:max-w-4xl md:px-0 sm:px-[45px] grid xl:grid-cols-3 grid-cols-1">
        <div className="mt-8 flex xl:hidden">
          <button
            onClick={() => active > 0 && setActive(active - 1)}
            className="w-1/2 mr-2 py-2 hover:text-orange-500"
          >
            <span>Quay lại</span>
          </button>
          <button
            disabled={bookingData?.selectedSeats?.length === 0}
            onClick={handleNextStep}
            className={`
    ${
      bookingData?.selectedSeats?.length > 0
        ? "bg-orange-500 hover:bg-orange-400 animate-fade"
        : "cursor-not-allowed bg-gray-500 animate-fade"
    }
    w-1/2 ml-2 py-2 text-white border rounded-md
  `}
          >
            <span>
              {active === steps.length - 2 ? "Thanh toán" : "Tiếp tục"}
            </span>
          </button>
        </div>
      </div>
      {showPopup && (
        <ConfirmPaymentMethod
          paymentMethod={paymentMethod}
          data={{
            movie: bookingData?.selectedSchedule?.movie,
            schedule: bookingData?.selectedSchedule,
            seat: bookingData?.selectedSeats,
          }}
          onClose={handleClosePopup}
        />
      )}
    </div>
  );
};

export default Booking;
