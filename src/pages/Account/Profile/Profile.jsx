import React, { useState } from "react";
import SignIn from "../../Auth/SignIn";
import DynamicInputPopup from "../Popup/DynamicInputPopup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCalendar,
  faEnvelope,
  faLock,
  faMobile,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { formatDate } from "../../../systems/ultis/FormatDate.js";

const Profile = () => {
  // const
  const [openPopup, setOpenPopup] = useState("");
  const handleOpenPopup = (type) => {
    setOpenPopup(type);
  };

  return (
    <div className="bg-white px-4 py-4 rounded mt-4 min-h-[350px] md:px-0 md:py-6 lg:px-6">
      {openPopup && (
        <DynamicInputPopup
          type={openPopup}
          onClose={() => setOpenPopup()}
          initialAnimation="animate-fade  animate-duration-[500ms]  animate-ease-in-out"
        />
      )}
      <form action="">
        <div className="grid grid-cols-1 gap-1 md:grid-cols-2 md:gap-4">
          <div className="col-span-1">
            <label htmlFor="Name">Họ và tên</label>
            <div className="w-full my-1 relative h-auto border inline-flex items-center min-w-0 text-[14px] bg-[#ECECEC] rounded-md transition-all duration-300 grayscale opacity-70 cursor-not-allowed">
              <FontAwesomeIcon className="absolute left-2" icon={faUser} />
              <input
                type="text"
                value={JSON.parse(localStorage.getItem("customer"))?.full_name}
                className="bg-transparent w-full h-9 px-8 focus:ring-2 focus:outline-blue-500 focus:rounded-md grayscale opacity-70 cursor-not-allowed"
                disabled
              />
            </div>
          </div>
          <div className="col-span-1">
            <label htmlFor="Date of birth">Ngày sinh</label>
            <div className="w-full my-1 relative h-auto border inline-flex items-center min-w-0 text-[14px] bg-[#ECECEC] rounded-md transition-all duration-300 grayscale opacity-70 cursor-not-allowed">
              <FontAwesomeIcon className="absolute left-2" icon={faCalendar} />
              <input
                type="text"
                value={formatDate(
                  JSON.parse(localStorage.getItem("customer"))?.birthday
                )}
                className="bg-transparent w-full h-9 px-8 focus:ring-2 focus:outline-blue-500 focus:rounded-md grayscale opacity-70 cursor-not-allowed"
                disabled
              />
            </div>
          </div>
          <div className="col-span-1">
            <label htmlFor="Email">Email</label>
            <div className="w-full my-1 relative h-auto border inline-flex items-center min-w-0 text-[14px] bg-[#ECECEC] rounded-md transition-all duration-300 opacity-70 cursor-not-allowed">
              <FontAwesomeIcon className="absolute left-2" icon={faEnvelope} />
              <input
                type="text"
                name="email"
                value={JSON.parse(localStorage.getItem("customer"))?.email}
                className="bg-transparent w-full h-9 px-8 focus:ring-2 focus:outline-blue-500 focus:rounded-md grayscale opacity-70"
                disabled
              />
              {/* <button
                onClick={() => handleOpenPopup("email")}
                className="absolute right-2 cursor-pointer grayscale-0 opacity-100 text-primary z-100 text-[12px] font-bold"
              >
                <span className="text-[#F58020]">Thay đổi</span>
              </button> */}
            </div>
          </div>
          <div className="col-span-1">
            <label htmlFor="Phone Numer">Số điện thoại</label>
            <div className="w-full my-1 relative h-auto border inline-flex items-center min-w-0 text-[14px] bg-[#ECECEC] rounded-md transition-all duration-300 grayscale opacity-70 cursor-not-allowed">
              <FontAwesomeIcon className="absolute left-2" icon={faMobile} />
              <input
                type="text"
                value={JSON.parse(localStorage.getItem("customer"))?.phone}
                className="bg-transparent w-full h-9 px-8 focus:ring-2 focus:outline-blue-500 focus:rounded-md grayscale opacity-70 cursor-not-allowed"
                disabled
              />
            </div>
          </div>
          <div className=" flex flex-wrap justify-start items-center md:mt-6">
            <div className="flex items-center mr-4 grayscale opacity-70 cursor-not-allowed">
              <input
                disabled
                type="radio"
                className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 grayscale opacity-70 cursor-not-allowed pointer-events-none"
                value="Male"
                checked
              />
              <label className="ml-2 text-sm font-medium text-gray-900">
                Nam
              </label>
            </div>
            <div className="flex items-center opacity-70 cursor-not-allowed">
              <input
                disabled=""
                type="radio"
                className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 grayscale opacity-70 cursor-not-allowed pointer-events-none"
                value="Female"
              />
              <label className="ml-2 text-sm font-medium text-gray-900">
                Nữ
              </label>
            </div>
          </div>
          <div className="col-span-1">
            <label htmlFor="Password">Mật khẩu</label>
            <div className="w-full my-1 relative h-auto border inline-flex items-center min-w-0 text-[14px] bg-[#ECECEC] rounded-md transition-all duration-300 opacity-70 cursor-not-allowed">
              <FontAwesomeIcon className="absolute left-2" icon={faLock} />
              <input
                type="password"
                name="password"
                value="a@gmail.com"
                className="bg-transparent w-full h-9 px-8 focus:ring-2 focus:outline-blue-500 focus:rounded-md grayscale opacity-70"
                disabled
              />
              <button
              type="button"
                onClick={() => handleOpenPopup("password")}
                className="absolute right-2 cursor-pointer grayscale-0 opacity-100 text-primary z-100 text-[12px] font-bold"
              >
                <span className="text-[#F58020]">Thay đổi</span>
              </button>
            </div>
          </div>
        </div>
        <div className="mt-4 text-right">
          <button
            type="submit"
            style={{ backgroundColor: "#F58020" }}
            className="inline-block rounded text-white py-2 px-6 w-[120px] h-[40px]"
          >
            Cập nhật{" "}
          </button>
        </div>
      </form>
    </div>
  );
};

export default Profile;
