import { faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import Notification from "./Notification/Notification";
import Policy from "./Policy/Policy";
import Profile from "./Profile/Profile";
import Reward from "./Reward/Reward";
import Transaction from "./Transaction/Transaction";
const AccountStructure = [
  {
    label: "Lịch sử giao dịch",
    link: "#transaction",
  },
  {
    label: "Thông tin cá nhân",
    link: "#profile",
  },
  {
    label: "Thông báo",
    link: "#notification",
  },
  {
    label: "Quà tặng",
    link: "#policy",
  },
  {
    label: "Chính sách",
    link: "#reward",
  },
];
const Account = () => {
  const location = useLocation();
  const [activeTab, setActiveTab] = useState(location.hash);
  const navigate = useNavigate();
  useEffect(() => {
    if (!location.hash) {
      navigate("#profile", { replace: true });
    } else {
      setActiveTab(location.hash);
    }
  }, [location.hash, navigate]);
  const renderContent = () => {
    switch (activeTab) {
      case "#profile":
        return <Profile />;
      case "#transaction":
        return <Transaction />;
      case "#policy":
        return <Policy />;
      case "#reward":
        return <Reward />;
      case "#notification":
        return <Notification />;
      default:
        return <Profile />;
    }
  };

  return (
    <div style={{ backgroundColor: "whitesmoke" }} className="">
      <div className="grid-cols-1 md:grid-cols-4 lg:grid-cols-3 my-0 mx-auto screen1390:max-w-screen-xl xl:max-w-screen-screen1200 lg:max-w-[1280px] md:py-10 py-5 md:gap-[30px] xl:gap-16 px-4 md:px-[45px] xl:px-0 xl:grid">
        <div className="md:col-span-2 lg:col-span-1">
          <div className="bg-white px-6 md:p-4  xl:px-6 xl:py-4  xl:shadow-2xl rounded mb-8">
           {}
            <div className="border-0">
              <ul className=" *:cursor-pointer ">
                <li className="py-4">
                  <a
                    className="flex justify-between items-center text-sm font-bold not-italic text-[#034EA2]"
                    href=""
                  >
                    <span>
                      <strong className="text-[#333333]">
                        HOTLINE hỗ trợ :
                      </strong>
                      00000000 (9:00 - 22:00)
                    </span>
                    <span>
                      <FontAwesomeIcon icon={faAngleRight} />
                    </span>
                  </a>
                </li>
                <li className="py-4 border-y border-[#ECECEC]">
                  <a
                    className="flex justify-between items-center text-sm font-bold not-italic text-[#034EA2]"
                    href=""
                  >
                    <span>
                      <strong className="text-[#333333]">Email:</strong>
                      hotro@galaxystudio.vn
                    </span>
                    <span>
                      <FontAwesomeIcon icon={faAngleRight} />
                    </span>
                  </a>
                </li>
                <li className="py-4">
                  <a
                    className="flex justify-between items-center text-sm font-bold not-italic text-[#034EA2]"
                    href=""
                  >
                    <strong className="text-[#333333]">
                      Câu hỏi thường gặp
                    </strong>
                    <span>
                      <FontAwesomeIcon icon={faAngleRight} />
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="md:col-span-2 lg:col-span-2 col-span-2">
          <div className="relative">
            <ul
              className="flex  flex-nowrap mb-0 list-none flex-row whitespace-nowrap"
              role="tablist"
            >
              {AccountStructure.map((value, index) => (
                <li
                  key={index}
                  className="px-2 pb-2 flex-auto text-center transition-all duration-300 cursor-pointer relative"
                >
                  <Link
                    to={value.link}
                    className={`${
                      activeTab !== value.link
                        ? "text-gray-400"
                        : "text-blue-900"
                    } `}
                  >
                    {value.label}
                  </Link>
                  <div
                    className={`w-full h-[2px] mt-4 gap-x-[28px] gap-y-[24px] text-center bg-blue-500 duration-300 transition-all ease-in-out absolute bottom-0
                        ${
                          activeTab !== value.link
                            ? "bg-gray-400"
                            : "bg-blue-900 "
                        } `}
                  ></div>
                </li>
              ))}
            </ul>
          </div>
          {renderContent()}
        </div>
      </div>
    </div>
  );
};

export default Account;
