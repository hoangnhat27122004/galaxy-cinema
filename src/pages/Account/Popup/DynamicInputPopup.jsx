import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { joiResolver } from "@hookform/resolvers/joi";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import FormField from "../../../components/UI/FormField.jsx";
import useAnimation from "../../../hooks/customHooks/useAnimation.jsx";
import { changePasswordValidate } from "../../../validations/Auth.js";
import { changePassword } from "../../../services/auth.js";
const DynamicInputPopup = ({ type, onClose, initialAnimation }) => {
  const [animate, triggerJumpOut] = useAnimation(initialAnimation, onClose);
  const [errorMessage, setErrorMessage] = useState("");
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    watch,
  } = useForm({ resolver: joiResolver(changePasswordValidate) });

  const newPassword = watch("newPassword");
  const oldPassword = watch("oldPassword");
  const onSubmit = async (data) => {
    const customer = localStorage.getItem("customer");
    const customerObj = customer ? JSON.parse(customer) : {};
    const email = customerObj.email || "";
    const formData = {
      ...data,
      email: email,
    };

    try {
      const res = await changePassword(formData);
    } catch (error) {
      if (error.response) {
        setErrorMessage(
          error.response.data.message || "Có lỗi xảy ra khi thay đổi mật khẩu"
        );
      } else if (error.request) {
        setErrorMessage("Không thể kết nối đến server. Vui lòng thử lại sau.");
      } else {
        setErrorMessage("Có lỗi xảy ra. Vui lòng thử lại.");
      }
      console.error("Error changing password:", error);
    }
  };
  useEffect(() => {
    if (errorMessage) {
      setErrorMessage(null);
    }
  }, [oldPassword, newPassword]);
  return (
    <div
      className={`fixed top-0 left-0 w-full h-full flex items-center justify-center z-50 transition-all ${animate}`}
    >
      <div className="bg-black bg-opacity-50 fixed inset-0 z-[-1]"></div>
      <div>
        {type === "password" ? (
          <div className="h-full w-full text-center">
            <div className="min-w-[375px] max-w-[400px] rounded-md inline-block align-middle bg-white m-5 p-5 relative">
              <div>
                <div>
                  <div className="mb-4">
                    <h5 className="text-lg font-bold not-italic py-1 capitalize">
                      Thay đổi Mật khẩu
                    </h5>
                  </div>
                  <div>
                    <form onSubmit={handleSubmit(onSubmit)}>
                      <FormField
                        label="Mật khẩu hiện tại"
                        type="password"
                        placeholder="Nhập mật khẩu hiện tại"
                        error={errors.oldPassword}
                        {...register("oldPassword")}
                      />
                      <FormField
                        label="Mật khẩu mới"
                        type="password"
                        placeholder="Nhập mật khẩu mới"
                        error={errors.newPassword}
                        {...register("newPassword")}
                      />
                      {errorMessage && (
                        <div className="my-2 text-red-500 animate-fade">
                          {errorMessage}
                        </div>
                      )}
                      <button
                        className="rounded-md hover:bg-[#e38601] transition-all duration-300 min-w-[135px] w-full focus:outline-none focus:ring-[#e38601] text-sm text-center inline-flex items-center dark:hover:bg-[#e38601] dark:focus:ring-[#e38601] justify-center text-white bg-[#f26b38] h-full px-5 py-2.5 uppercase mt-4"
                        type="submit"
                      >
                        Tiếp tục
                      </button>
                    </form>
                  </div>
                </div>
              </div>
              <button
                className="absolute top-[14px] right-[14px] border-0 p-0 cursor-pointer bg-transparent flex"
                onClick={() => {
                  onClose();
                  triggerJumpOut();
                }}
              >
                <span className="inline-flex bg-gray-200 rounded-full w-[24px] h-[24px] items-center justify-center">
                  <FontAwesomeIcon icon={faXmark} color="grey" />
                </span>
              </button>
            </div>
          </div>
        ) : (
          <div className="text-center min-w-[315px] rounded-md max-w-3xl inline-block align-middle bg-white m-5 p-5 relative overflow-y-auto">
            <div className="mb-4">
              <h5 className="text-lg font-bold not-italic py-1 capitalize">
                Thay đổi Email
              </h5>
              <span>
                Vui lòng cung cấp email mới, chúng tôi sẽ gửi <br /> mã xác thực
                cho bạn!
              </span>
            </div>
            <div>
              <form action="">
                <FormField
                  label="Email mới"
                  type="email"
                  placeholder="Nhập email mới"
                />
                <button
                  className="rounded-md hover:bg-[#e38601] transition-all duration-300 min-w-[135px] w-full focus:outline-none focus:ring-[#e38601] text-sm text-center inline-flex items-center dark:hover:bg-[#e38601] dark:focus:ring-[#e38601] justify-center text-white bg-[#f26b38] h-full px-5 py-2.5 uppercase mt-4"
                  type="submit"
                >
                  Tiếp tục
                </button>
              </form>
            </div>
            <button
              className="absolute top-[14px] right-[14px] border-0 p-0 cursor-pointer bg-transparent flex"
              onClick={() => {
                onClose();
                triggerJumpOut();
              }}
            >
              <span className="inline-flex bg-gray-200 rounded-full w-[24px] h-[24px] items-center justify-center">
                <FontAwesomeIcon icon={faXmark} color="grey" />
              </span>
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default DynamicInputPopup;
