import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { joiResolver } from "@hookform/resolvers/joi";
import React, { useState } from "react";
import "react-calendar/dist/Calendar.css";
import DatePicker from "react-date-picker";
import "react-date-picker/dist/DatePicker.css";
import { Controller, useForm } from "react-hook-form";
import FormField from "../../components/UI/FormField";
import { signUp } from "../../services/auth.js";
import { formatBirthDate } from "../../systems/ultis/FormatDate.js";
import { signUpValidator } from "../../validations/Auth.js";
import { useNavigate } from "react-router-dom";
import messageService from "../../components/base/Message/Message.jsx";

const SignUp = ({ onClose, onSwitch }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
    setError,
  } = useForm({
    resolver: joiResolver(signUpValidator),
  });
  const [generalError, setGeneralError] = useState(null);

  const onSubmit = async (data) => {
    try {
      const res = await signUp({
        ...data,
        birthday: formatBirthDate(data.birthday),
      });
      messageService.success("Đăng ký tài khoản thành công !");
      onSwitch();
    } catch (error) {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message
      ) {
        const errorMessage = error.response.data.message;
        if (errorMessage.includes("Email")) {
          setError("email", {
            type: "manual",
            message: errorMessage,
          });
        } else if (errorMessage.includes("Số")) {
          setError("phone", {
            type: "manual",
            message: errorMessage,
          });
        } else {
          setGeneralError(errorMessage);
        }
      } else {
        setGeneralError("Có lỗi xảy ra. Vui lòng thử lại.");
      }
    }
  };

  return (
    <React.Fragment>
      <FontAwesomeIcon
        onClick={() => onClose()}
        icon={faXmark}
        className="absolute right-5 cursor-pointer"
      />
      <img
        src="https://www.galaxycine.vn/_next/static/media/icon-login.fbbf1b2d.svg"
        alt=""
        className="mx-auto"
      />
      <p className="capitalize font-medium text-lg text-center my-2">
        đăng ký tài khoản
      </p>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="mb-3">
          <FormField
            label="Họ và tên"
            type="text"
            placeholder="Nhập họ và tên của bạn"
            error={errors.full_name}
            {...register("full_name")}
          />
        </div>
        <div className="mb-3">
          <FormField
            label="Email"
            type="email"
            placeholder="you@gmail.com"
            error={errors.email}
            {...register("email")}
          />
        </div>
        <div className="mb-3">
          <FormField
            label="Số điện thoại"
            type="numeric"
            placeholder="Nhập số điện thoại của bạn"
            error={errors.phone}
            {...register("phone")}
          />
        </div>
        <div className="mb-3">
          <label
            htmlFor="gender"
            className="block mb-2 text-sm text-gray-600 dark:text-gray-400"
          >
            Giới tính
          </label>
          <div className="flex gap-3">
            <div>
              <input
                type="radio"
                id="male"
                {...register("gender")}
                value="male"
                className="mr-2"
                defaultChecked
              />
              <label htmlFor="male" className="text-gray-700">
                Nam
              </label>
            </div>
            <div>
              <input
                type="radio"
                id="female"
                {...register("gender")}
                value="female"
                className="mr-2"
                defaultChecked
              />
              <label htmlFor="female" className="text-gray-700">
                Nữ
              </label>
            </div>
            <div>
              <input
                type="radio"
                id="unknown"
                {...register("gender")}
                value="unknown"
                className="mr-2"
                defaultChecked
              />
              <label htmlFor="unknown" className="text-gray-700">
                Chưa xác định
              </label>
            </div>
          </div>
          {errors.gender && (
            <span className="text-red-500 mt-1">{errors.gender.message}</span>
          )}
        </div>
        <div className="mb-3">
          <label
            htmlFor="birthday"
            className="block mb-2 text-sm text-gray-600 dark:text-gray-400"
          >
            Ngày sinh
          </label>
          <Controller
            name="birthday"
            control={control}
            rules={{ required: "Birth date is required" }}
            defaultValue={null}
            render={({ field, fieldState: { error } }) => (
              <>
                <DatePicker
                  onChange={(date) => {
                    if (date) {
                      const adjustedDate = new Date(
                        date.getTime() + date.getTimezoneOffset() * 60000
                      );
                      field.onChange(adjustedDate);
                    } else {
                      field.onChange(null);
                    }
                  }}
                  value={field.value}
                  format="dd-MM-y"
                  className={`
                    ${error ? "border-red-500" : "border-gray-300"}
                    w-full px-3 py-2 placeholder-gray-300 border rounded-md
                    focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300
                    dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600
                    dark:focus:ring-gray-900 dark:focus:border-gray-500
                  `}
                />
                {error && (
                  <span className="text-red-500 text-sm mt-1">
                    {error.message}
                  </span>
                )}
              </>
            )}
          />
        </div>
        <div className="mb-3">
          <FormField
            label="Mật khẩu"
            type="password"
            placeholder="********"
            error={errors.password}
            {...register("password")}
          />
        </div>
        {generalError && (
          <div className="mb-3 text-red-500">{generalError}</div>
        )}
        <div className="mb-6">
          <button
            type="submit"
            className="w-full px-3 py-4 text-white bg-blue-500 rounded-md focus:bg-blue-600 focus:outline-none"
          >
            Đăng ký
          </button>
        </div>
      </form>
      <p className="text-sm text-center text-gray-400">
        Bạn đã có tài khoản ?
        <button
          onClick={onSwitch}
          className="text-blue-400 focus:outline-none focus:underline focus:text-blue-500 dark:focus:border-blue-800 ml-2"
        >
          Đăng nhập
        </button>
        .
      </p>
    </React.Fragment>
  );
};

export default SignUp;
