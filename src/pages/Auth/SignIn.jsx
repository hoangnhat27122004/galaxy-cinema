/* eslint-disable react/prop-types */
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { joiResolver } from "@hookform/resolvers/joi";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import FormField from "../../components/UI/FormField";
import { signIn } from "../../services/auth";
import { signInValidator } from "../../validations/Auth";
import messageService from "../../components/base/Message/Message";
import { useAuth } from "../../context/authContext";
const SignIn = ({
  onClose,
  showSpecialMessage,
  onSwitch,
  onShowBookingForm,
  onForgotPassword,
}) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    watch,
  } = useForm({
    resolver: joiResolver(signInValidator),
  });
  const { setIsLoggedIn } = useAuth();
  const [error, setError] = useState();
  const email = watch("email");
  const password = watch("password");
  const onSubmit = async (data) => {
    try {
      await signIn(data);
      setIsLoggedIn(true);
      messageService.success("Đăng nhập thành công !");
      onClose();
    } catch (error) {
      setError(error.response.data.message);
    }
  };

  useEffect(() => {
    if (error) {
      setError(null);
    }
  }, [email, password]);
  return (
    <React.Fragment>
      <FontAwesomeIcon
        onClick={() => onClose()}
        icon={faXmark}
        className="absolute right-5 cursor-pointer"
      />
      <img
        src="https://www.galaxycine.vn/_next/static/media/icon-login.fbbf1b2d.svg"
        alt=""
        className="mx-auto"
      />
      <p className="capitalize font-medium text-lg text-center my-2">
        đăng nhập tài khoản
      </p>
      <div className="container mx-auto">
        <div className="max-w-md mx-auto mt-10">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="mb-6">
              <FormField
                label="Email"
                type="email"
                placeholder="Nhập email của bạn"
                error={errors.email}
                {...register("email")}
              />
            </div>
            <div className="mb-6">
              <FormField
                label="Mật khẩu"
                type="password"
                placeholder="********"
                error={errors.password}
                {...register("password")}
              />
            </div>
            {error && (
              <div className="mb-6 text-red-500 animate-fade">{error}</div>
            )}
            <div className="">
              <button
                type="submit"
                className="capitalize w-full px-3 py-4 text-white bg-indigo-500 rounded-md focus:bg-indigo-600 focus:outline-none"
              >
                Đăng nhập
              </button>
            </div>
            <div className="w-full my-3 text-left">
              <button
                onClick={onForgotPassword}
                className="text-sm  text-indigo-400 hover:underline focus:outline-none focus:text-indigo-500 dark:hover:text-indigo-300"
              >
                Quên mật khẩu ?
              </button>
            </div>
          </form>

          <p className="text-sm text-center border-t-2 pt-4 pb-2 text-gray-400">
            Bạn chưa có tài khoản ?
            <button
              onClick={onSwitch}
              className="text-indigo-400 hover:underline focus:text-indigo-500 dark:focus:border-indigo-800 ml-2"
            >
              Đăng ký
            </button>
          </p>
          {/* {showSpecialMessage && (
            <>
              <div className="flex items-center justify-center pb-2 ">
                <span className="inline-block border w-full h-[2px] border-orange-600"></span>
                <p className="text-sm text-center px-2 text-gray-400">Hoặc</p>
                <span className="inline-block border w-full h-[2px] border-sky-600"></span>
              </div>
              <p className="text-sm text-justify text-gray-400">
                Bạn có thể tiếp tục mà không cần đăng nhập hoặc đăng ký bằng
                cách cung cấp email và số điện thoại của bạn. Nếu email và số
                điện thoại khớp với dữ liệu của chúng tôi, chúng tôi sẽ tự động
                truy xuất thông tin tài khoản của bạn và tặng{" "}
                <Link className="text-indigo-400 hover:underline focus:text-indigo-500 dark:focus:border-indigo-800">
                  điểm thưởng
                </Link>
                .{" "}
                <button
                  onClick={() => onShowBookingForm()}
                  className="text-indigo-400 hover:underline focus:text-indigo-500 dark:focus:border-indigo-800"
                >
                  Bấm vào đây
                </button>{" "}
                để tiếp tục.
              </p>
            </>
          )} */}
        </div>
      </div>
    </React.Fragment>
  );
};

export default SignIn;
