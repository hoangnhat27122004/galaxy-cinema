import { joiResolver } from "@hookform/resolvers/joi";
import React from "react";
import { useForm } from "react-hook-form";
import { forgotPasswordValidate } from "../../validations/Auth";
import FormField from "../../components/UI/FormField";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { forgotPassword } from "../../services/auth";

const ForgotPassword = ({ onClose, onSwitch }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    watch,
  } = useForm({
    resolver: joiResolver(forgotPasswordValidate),
  });
  const onSubmit = async (data) => {
    try {
      await forgotPassword(data);
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div>
      <button className="underline cursor-pointer" onClick={onSwitch}>
        Quay lại đăng nhập
      </button>
      <FontAwesomeIcon
        onClick={() => onClose()}
        icon={faXmark}
        className="absolute right-5 cursor-pointer"
      />
      <img
        src="https://www.galaxycine.vn/_next/static/media/icon-login.fbbf1b2d.svg"
        alt=""
        className="mx-auto"
      />
      <p className="capitalize font-medium text-lg text-center my-2">
        quên mật khẩu?
      </p>
      <p class="text-sm text-black-10 mb-2">
        Vui lòng cung cấp email đăng nhập, chúng tôi sẽ gửi link kích hoạt cho
        bạn.
      </p>
      <div className="container mx-auto">
        <div className="max-w-md mx-auto">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="mb-6">
              <FormField
                label="Email"
                type="email"
                placeholder="Nhập email của bạn"
                error={errors.email}
                {...register("email")}
              />
            </div>
            <div className="">
              <button
                type="submit"
                className="capitalize w-full px-3 py-4 text-white bg-indigo-500 rounded-md focus:bg-indigo-600 focus:outline-none"
              >
                Cấp lại mật khẩu
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ForgotPassword;
