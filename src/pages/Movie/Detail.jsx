import React, { useRef, useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { isToday, parseISO } from "date-fns";
import MovieCard from "../../components/UI/MovieCard";
import MovieTab from "../../components/UI/MovieTab";
import Preview from "../../components/UI/PreviewVideo";
import useOutsideClick from "../../hooks/customHooks/useOutsideClick";
import usePreviewHiddenScroll from "../../hooks/customHooks/usePreviewHiddenScroll";
import {
  dayAndMonth,
  formatDate,
  formatSchedule,
} from "../../systems/ultis/FormatDate";
import SignIn from "../Auth/SignIn";
import SignUp from "../Auth/SignUp";
import BookingWithoutLogin from "../Booking/BookingWithoutLogin";
import useMovieQuery from "../../hooks/Movie/useMovieQuery";
import useScheduleQuery from "../../hooks/Schedule/useScheduleQuery";
import Spin from "../../components/base/Loading/Spin";
import { useAuth } from "../../context/authContext";

const MovieDetail = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const { data: movie, isLoading } = useMovieQuery(id);
  const { data: schedules } = useScheduleQuery(id);
  const [preview, setPreview] = useState(false);
  const [selectSchedule, setSelectSchedule] = useState(null);
  const [sortedSchedules, setSortedSchedules] = useState([]);
  const [openForm, setOpenForm] = useState(false);
  const [isSignIn, setIsSignIn] = useState(true);
  const [showBookingForm, setShowBookingForm] = useState(false);
  const { isLoggedIn } = useAuth();

  const ref = useRef(null);

  useOutsideClick(ref, () => {
    if (preview) setPreview(false);
    if (openForm) {
      setOpenForm(false);
      setIsSignIn(true);
    }
  });
  usePreviewHiddenScroll(preview);
  usePreviewHiddenScroll(openForm);

  const groupSchedulesByDate = (schedules) => {
    return schedules.reduce((acc, schedule) => {
      const date = schedule.show_date;
      if (!acc[date]) {
        acc[date] = [];
      }
      acc[date].push(schedule);
      return acc;
    }, {});
  };

  useEffect(() => {
    if (schedules && schedules.schedules) {
      const grouped = groupSchedulesByDate(schedules.schedules);
      const sortedDates = Object.keys(grouped).sort(
        (a, b) => parseISO(a).getTime() - parseISO(b).getTime()
      );
      setSortedSchedules(
        sortedDates.map((date) => ({
          date,
          schedules: grouped[date].sort((a, b) =>
            a.start_time.localeCompare(b.start_time)
          ),
        }))
      );

      const todayIndex = sortedDates.findIndex((date) =>
        isToday(parseISO(date))
      );
      setSelectSchedule(todayIndex !== -1 ? todayIndex : 0);
    }
  }, [schedules]);

  const handleBooking = (schedule) => {
    if (isLoggedIn) {
      const selectedDate = schedule.show_date;
      const schedulesForSelectedDate = schedules.schedules.filter(
        (s) => s.show_date === selectedDate
      );

      const bookingData = {
        selectedSchedule: schedule,
        schedulesForDate: schedulesForSelectedDate,
        movie: movie,
        selectedSeats: [],
      };

      localStorage.setItem("quote", JSON.stringify(bookingData));
      navigate(`/booking/${schedule.movie_id}`);
    } else {
      setOpenForm(true);
    }
  };

  const handleShowBookingForm = () => {
    setShowBookingForm(true);
    setOpenForm(false);
  };

  if (isLoading) {
    return <Spin />;
  }

  return (
    <React.Fragment>
      {preview && <Preview src={movie.trailer} ref={ref} />}
      {openForm && (
        <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-50 transition-all bg-black bg-opacity-45">
          <div
            className={`
              ${
                !isSignIn
                  ? "ssm:h-full ssm:overflow-y-auto ssm:overflow-x-hidden"
                  : ""
              }
            `}
            ref={ref}
          >
            <div className="bg-white max-w-[400px] ssm:min-w-[360px] min-w-[400px] rounded-md p-8 relative m-2">
              {isSignIn ? (
                <SignIn
                  onClose={() => setOpenForm(false)}
                  onSwitch={() => setIsSignIn(!isSignIn)}
                  showSpecialMessage
                  onShowBookingForm={handleShowBookingForm}
                />
              ) : (
                <SignUp
                  onClose={() => setOpenForm(false)}
                  onSwitch={() => setIsSignIn(!isSignIn)}
                />
              )}
            </div>
          </div>
        </div>
      )}
      {/* {showBookingForm && (
        <BookingWithoutLogin
          onClose={() => {
            setShowBookingForm(false);
            setOpenForm(true);
          }}
        />
      )} */}
      <div className="relative bg-black w-full flex justify-center">
        <div className="relative">
          <img src={movie.image_landscape.url} alt="" />
          <button
            onClick={() => setPreview(!preview)}
            className="absolute top-[50%] left-[50%] -translate-x-2/4 -translate-y-2/4"
          >
            <img
              width={64}
              height={64}
              src="https://www.galaxycine.vn/_next/static/media/button-play.2f9c0030.png"
              alt=""
            />
          </button>
        </div>
      </div>
      <div className="grid lg:grid-cols-7 grid-cols-1 my-0 mx-auto container lg:max-w-[1280px] gap-8 py-7 md:px-4 px-4">
        <div className="col-span-5 w-full ">
          <MovieCard data={movie} />
          <div className="">
            <span className="border-l-4 border-blue-800 lg:text-lg font-medium text-sm capitalize">
              &ensp; Nội dung Phim
            </span>
            <p className="mt-3 text-justify">{movie.description}</p>
          </div>
          <div>
            <span className="border-l-4 border-blue-800 lg:text-lg font-medium text-sm capitalize">
              &ensp; Lịch chiếu
            </span>
            {!schedules?.message ? (
              <>
                <div className="flex gap-3 mx-10 my-5 overflow-x-auto">
                  {sortedSchedules.map((schedule, i) => (
                    <div
                      key={schedule.date}
                      className={`flex flex-col items-center text-gray-500 p-2 rounded-md w-[80px] cursor-pointer ${
                        selectSchedule === i
                          ? "bg-blue-800 text-white  animate-fade"
                          : ""
                      }`}
                      onClick={() => setSelectSchedule(i)}
                    >
                      <span>{formatSchedule(schedule.date)}</span>
                      <span>{dayAndMonth(schedule.date)}</span>
                    </div>
                  ))}
                </div>
                <div className="border-t-2 border-blue-800" />
                <div className="grid grid-cols-5 gap-2 items-center my-2">
                  <span className="col-span-1 capitalize">Thời gian chiếu</span>
                  <div className="col-span-4 flex flex-wrap gap-3">
                    {selectSchedule !== null &&
                      sortedSchedules[selectSchedule] &&
                      sortedSchedules[selectSchedule].schedules.map(
                        (schedule, index) => (
                          <button
                            key={index}
                            onClick={() => handleBooking(schedule)}
                          >
                            <div className="border border-gray-300 rounded-sm px-6 py-1 animate-fade">
                              {schedule.start_time.slice(0, 5)}
                            </div>
                          </button>
                        )
                      )}
                  </div>
                </div>
              </>
            ) : (
              <>
                <div className="my-4">Phim chưa có lịch chiếu</div>
              </>
            )}
          </div>
        </div>
        <div className="col-span-2 lg:block hidden">
          <MovieTab />
        </div>
      </div>
    </React.Fragment>
  );
};

export default MovieDetail;
