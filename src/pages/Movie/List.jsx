import {
  faCirclePlay,
  faStar,
  faTicket,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import useOutsideClick from "../../hooks/customHooks/useOutsideClick";
import usePreviewHiddenScroll from "../../hooks/customHooks/usePreviewHiddenScroll";
import Preview from "../../components/UI/PreviewVideo";

const MovieList = ({ data }) => {
  const [isDesktop, setIsDesktop] = useState(window.innerWidth > 1024);
  const [preview, setPreview] = useState(false);
  const [currentMovie, setCurrentMovie] = useState(null);
  const previewRef = useRef(null);

  useOutsideClick(previewRef, () => {
    if (preview) setPreview(false);
  });
  usePreviewHiddenScroll(preview);

  useEffect(() => {
    const handleResize = () => {
      setIsDesktop(window.innerWidth > 1024);
    };
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const handlePreview = (movie) => {
    setCurrentMovie(movie);
    setPreview(true);
  };

  return (
    <>
      {data.map((movie, index) => (
        <div className="relative m-2 animate-fade " key={index}>
          <Link to={`dat-ve/${movie.id}`} className="relative block">
            <img
              src={movie.image_portrait.url}
              alt={movie.name}
              className="rounded-lg"
            />
            <div
              className={`absolute inset-0 flex flex-col items-center justify-center opacity-0 hover:opacity-100 bg-black bg-opacity-50 transition rounded-lg`}
            >
              <button className="m-1 px-4 py-2 text-white rounded border-orange-600 bg-orange-600 hover:bg-orange-400 hover:border-orange-400">
                <FontAwesomeIcon icon={faTicket} /> Mua vé
              </button>
              <button
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  handlePreview(movie);
                }}
                className="m-1 px-5 py-2 bg-transparent border text-white rounded hover:bg-orange-400 hover:border-orange-400 hover:bg-opacity-50"
              >
                <FontAwesomeIcon icon={faCirclePlay} /> Trailer
              </button>
            </div>
            {/* <div
              style={{
                clipPath: "polygon(12px 0px, 100% 0px, 100% 100%, 0% 100%)",
              }}
              className="absolute flex items-center gap-1 right-0 bottom-16 bg-black bg-opacity-50 w-fit pr-2 pl-4 "
            >
              <FontAwesomeIcon icon={faStar} color="yellow" />
              <p className="text-white font-bold">8.7</p>
            </div> */}
          </Link>
          <div className="mt-2">
            <Link
              to={`dat-ve/${movie.id}`}
              className="text-lg font-medium block"
            >
              {movie.name}
            </Link>
          </div>
        </div>
      ))}
      {preview && currentMovie && (
        <Preview src={currentMovie.trailer} ref={previewRef} />
      )}
    </>
  );
};

export default MovieList;
