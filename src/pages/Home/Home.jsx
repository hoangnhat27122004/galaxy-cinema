import React, { useState, useRef } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import {
  banners,
  promotionalNew,
  selectDate,
  selectFilm,
  selectTime,
} from "../../../data.example";
import SelectOption from "../../components/UI/SelectOption";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import MovieList from "../Movie/List";
import { banner, promotionalNews } from "../../systems/ultis/Carousel";
import { Link } from "react-router-dom";
import useOutsideClick from "../../hooks/customHooks/useOutsideClick";
import usePreviewHiddenScroll from "../../hooks/customHooks/usePreviewHiddenScroll";
import useMovieQuery from "../../hooks/Movie/useMovieQuery";
import Spin from "../../components/base/Loading/Spin";

const Home = () => {
  const [preview, setPreview] = useState(false);
  const [activeTab, setActiveTab] = useState("nowShowing");
  const formRef = useRef(null);
  useOutsideClick(formRef, () => {
    if (preview) setPreview(false);
  });
  usePreviewHiddenScroll(preview);
  const { data: movies, isLoading } = useMovieQuery();

  const nowShowingMovies =
    movies?.movies?.filter((movie) => movie.status === "now_showing") || [];
  const upcomingMovies =
    movies?.movies?.filter((movie) => movie.status === "upcoming") || [];

  const handleTabClick = (tab) => {
    setActiveTab(tab);
  };

  return (
    <>
      <div className="relative">
        <Carousel
          autoPlay={true}
          autoPlaySpeed={2000}
          infinite={true}
          showDots={true}
          removeArrowOnDeviceType={["tablet", "mobile"]}
          responsive={banner}
          className="-z-20"
        >
          {banners.map((url, index) => (
            <img key={index} src={url.url} alt={url.title} />
          ))}
        </Carousel>
        {/* <div className="absolute left-1/2 transform -translate-x-1/2 -bottom-[24px] xl:grid grid-cols-11 border bg-white shadow-2xl rounded-sm max-w-6xl w-full hidden md:hidden lg:hidden z-50">
          <div className="col-span-3">
            <SelectOption title={"Chọn phim"} data={selectFilm} number={1} />
          </div>
          <div className="col-span-3">
            <SelectOption title={"Chọn ngày"} data={selectDate} number={2} />
          </div>
          <div className="col-span-3">
            <SelectOption title={"Chọn suất"} data={selectTime} number={3} />
          </div>
          <div className="col-span-2">
            <button className="w-full h-full bg-orange-500">
              Mua vé nhanh
            </button>
          </div>
        </div> */}
      </div>
      <div className="py-12 my-0 mx-auto xl:max-w-[1200px] lg:max-w-[1280px] px-[16px]">
        <div className="flex flex-row items-center gap-8 mt-10 -z-10">
          <span className="border-l-4 border-blue-800 text-2xl font-medium hidden lg:block uppercase">
            &ensp;Phim
          </span>
          <span
            className={`font-semibold relative cursor-pointer ${
              activeTab === "nowShowing"
                ? "text-blue-800  border-b-2 border-blue-800 animate-fade"
                : ""
            }`}
            onClick={() => handleTabClick("nowShowing")}
          >
            Đang chiếu
          </span>
          <span
            className={` font-semibold hover:text-blue-500 cursor-pointer ${
              activeTab === "upcoming"
                ? "text-blue-800 border-b-2 border-blue-800 animate-fade"
                : ""
            }`}
            onClick={() => handleTabClick("upcoming")}
          >
            Sắp chiếu
          </span>
        </div>
        <div className="mt-10 grid lg:grid-cols-4 grid-cols-2 gap-4">
          {isLoading ? (
            <div className="lg:col-span-4 col-span-2">
              <Spin />
            </div>
          ) : (
            <MovieList
              data={
                activeTab === "nowShowing" ? nowShowingMovies : upcomingMovies
              }
            />
          )}
        </div>
        {(activeTab === "nowShowing" && nowShowingMovies.length > 8) ||
        (activeTab === "upcoming" && upcomingMovies.length > 8) ? (
          <div className="text-center">
            <p className="border border-orange-500 py-2 px-10 inline-flex justify-center items-center text-orange-500 transition-all hover:bg-orange-500 hover:text-white cursor-pointer rounded-sm">
              Xem thêm
              <FontAwesomeIcon
                icon={faChevronRight}
                className="ml-2"
                size="sm"
              />
            </p>
          </div>
        ) : null}
      </div>
      <div className="py-12 my-0 mx-auto xl:max-w-[1200px] lg:max-w-[1280px] px-[16px] -z-10">
        <span className="border-l-4 border-blue-800 text-2xl font-medium hidden lg:block uppercase mb-10">
          &ensp;Tin tức khuyến mại
        </span>
        <Carousel
          autoPlay={true}
          autoPlaySpeed={2000}
          infinite={true}
          showDots={false}
          removeArrowOnDeviceType={["tablet", "mobile"]}
          responsive={promotionalNews}
        >
          {promotionalNew.map((v, index) => (
            <Link key={index}>
              <div className="mx-4 text-center">
                <img src={v.url} alt={v.title} />
                <p className="font-medium text-base mt-2">{v.title}</p>
              </div>
            </Link>
          ))}
        </Carousel>
      </div>
    </>
  );
};

export default Home;
