import { films } from "../../../data.example";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight, faTicket } from "@fortawesome/free-solid-svg-icons";
import { useEffect, useState } from "react";
import useMovieQuery from "../../hooks/Movie/useMovieQuery";
import Spin from "../base/Loading/Spin";

const MovieTab = () => {
  const [sliceMovies, setSliceMovies] = useState([]);
  const { data: movies, isLoading } = useMovieQuery();

  useEffect(() => {
    const nowShowingMovies =
      movies?.movies?.filter((movie) => movie.status === "now_showing") || [];
    const selected = nowShowingMovies.slice(0, 3);
    setSliceMovies(selected);
  }, [movies]);

  if (isLoading) return <Spin />;

  return (
    <div>
      <span className="border-l-4 border-blue-800 text-xl inline-block uppercase font-semibold">
        &ensp;phim đang chiếu
      </span>
      <div className="flex flex-col">
        {sliceMovies.map((movie, i) => (
          <div key={i} className="mt-2">
            <div className="relative mb-2">
              <Link to={`/dat-ve/${movie.id}`}>
                <img
                  className="object-cover rounded-lg"
                  src={movie.image_landscape.url}
                  alt={movie.name}
                />
              </Link>
              <div className="absolute inset-0 flex flex-col items-center justify-center opacity-0 hover:opacity-100 bg-black bg-opacity-50 transition rounded-lg">
                <Link
                  to={`/dat-ve/${movie.id}`}
                  className="m-1 px-4 py-2 text-white rounded border-orange-600 bg-orange-600 hover:bg-orange-400 hover:border-orange-400"
                >
                  <FontAwesomeIcon icon={faTicket} /> Mua vé
                </Link>
              </div>
            </div>
            <Link to={`/dat-ve/${movie.id}`} className="font-medium">
              {movie.name}
            </Link>
          </div>
        ))}
      </div>
      <div className="text-right mt-2">
        <Link to="/all-movies">
          <p className="border border-orange-500 py-2 px-8 inline-flex justify-center items-center text-orange-500 transition-all hover:bg-orange-500 hover:text-white cursor-pointer rounded-sm">
            Xem thêm
            <FontAwesomeIcon icon={faChevronRight} className="ml-2" size="sm" />
          </p>
        </Link>
      </div>
    </div>
  );
};

export default MovieTab;
