import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as React from "react";

const FormField = React.forwardRef(
  ({ className, type, label, error, ...props }, ref) => {
    const [showPassword, setShowPassword] = React.useState(false);

    return (
      <>
        <label
          htmlFor={label}
          className="block mb-2 text-sm text-gray-600 dark:text-gray-400 text-left "
        >
          {label}
        </label>
        <div className="relative">
          <input
            type={type === "password" && showPassword ? "text" : type}
            ref={ref}
            {...props}
            className={`${className}  ${
              error ? "border-red-500" : "border-gray-300"
            } w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500`}
          />
          {type === "password" && (
            <button
              type="button"
              onClick={() => setShowPassword(!showPassword)}
              className="absolute inset-y-0 right-0 pr-3 flex items-center text-gray-600 dark:text-gray-400"
            >
              <FontAwesomeIcon icon={!showPassword ? faEyeSlash : faEye} />
            </button>
          )}
        </div>
        {error && <span className="text-red-500 mt-1 animate-fade-down">{error.message}</span>}
      </>
    );
  }
);

export default FormField;
