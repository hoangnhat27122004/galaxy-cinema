import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";

const MenuComponent = ({ menuItems, title, activeMenu, setActiveMenu }) => {
  const toggleMenu = () => {
    if (activeMenu === title) {
      setActiveMenu(null);
    } else {
      setActiveMenu(title);
    }
  };

  return (
    <div className="group relative py-5">
      <span
        className="hover:text-orange-400 hover:cursor-pointer capitalize flex items-center"
        onClick={toggleMenu}
      >
        {title}
        <FontAwesomeIcon className="ml-2" icon={faChevronDown} size="2xs" />
      </span>

      <div
        className={`bg-white border border-white border-solid rounded lg:hidden ${
          activeMenu === title ? "block" : "hidden"
        }`}
        style={{
          boxShadow:
            "rgba(0, 0, 0, 0.08) 0px 6px 16px 0px, rgba(0, 0, 0, 0.12) 0px 3px 6px -4px, rgba(0, 0, 0, 0.05) 0px 9px 28px 8px",
        }}
      >
        <ul className="bg-white min-w-[200px] border border-white border-solid rounded">
          {menuItems.map((item, index) => (
            <li
              key={index}
              className="text-sm capitalize  py-2 px-[18px] text-black hover:text-[#f26b38] hover:pl-0.5 hover:border-l-4 hover:border-[#fd841f] hover:bg-[#fb770b1a] transition-all duration-300"
            >
              <Link to={item.path} className="capitalize grow ml-4">
                {item.label}
              </Link>
            </li>
          ))}
        </ul>
      </div>

      <div className="hidden lg:group-hover:block absolute top-[55px] left-0 z-[800] transition-all duration-300 ease-in-out shadow-xl capitalize">
        <ul className="bg-white min-w-[200px] text-center border border-white border-solid rounded">
          {menuItems.map((item, index) => (
            <li
              key={index}
              className="text-sm capitalize py-2 text-black hover:text-[#f26b38] hover:pl-0.5 hover:border-l-4 hover:border-[#fd841f] hover:bg-[#fb770b1a] transition-all duration-300"
            >
              <Link to={item.path} className="capitalize">
                {item.label}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default MenuComponent;
