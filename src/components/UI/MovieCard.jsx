import { faCalendar, faClock, faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useRef, useState } from "react";
import PreviewVideo from "./PreviewVideo";
import useOutsideClick from "../../hooks/customHooks/useOutsideClick";
import useResizeMobile from "../../hooks/customHooks/useResizeMobile";
import { formatDate } from "../../systems/ultis/FormatDate";

const MovieCard = ({ data, preview }) => {
  const isMobile = useResizeMobile();
  const [previewVideo, setPreviewVideo] = useState(false);
  const previewRef = useRef(null);
  useOutsideClick(previewRef, () => {
    if (previewVideo) setPreviewVideo(false);
  });
  return (
    <React.Fragment>
      {previewVideo && <PreviewVideo src={data.trailer} ref={previewRef} />}
      <div className="hidden md:flex gap-8 relative">
        <div
          className={`border relative border-white col-span-1 drop-shadow-2xl z-2 ${
            !preview && "lg:-translate-y-20 md:-translate-y-16 -translate-y-0"
          }`}
        >
          <img
            width={220}
            height={280}
            src={isMobile ? data.image_landscape.url : data.image_portrait.url}
            alt={data.name}
            className='border-2 rounded border-white lg:w-[280px] lg:h-[400px] md:w-[180px] md:h-[300px] w-full h-full object-cover duration-500 ease-in-out group-hover:opacity-100"
      scale-100 blur-0 grayscale-0)'
          />
          {preview && (
            <div className="absolute inset-0 flex items-center justify-center z-[6]">
              <button onClick={() => setPreviewVideo(!previewVideo)}>
                <img
                  width={64}
                  height={64}
                  src="https://www.galaxycine.vn/_next/static/media/button-play.2f9c0030.png"
                  alt=""
                />
              </button>
            </div>
          )}
        </div>
        <div className="col-span-2 lg:-translate-y-20 flex flex-col justify-end md:-translate-y-16 -translate-y-0 gap-2">
          <h3 className="font-medium text-3xl">{data.name}</h3>
          <span className="flex gap-2">
            <p className="font-normal">
              <FontAwesomeIcon icon={faClock} color="orange" /> {data.duration}{" "}
              phút
            </p>
            <p className="font-normal">
              <FontAwesomeIcon icon={faCalendar} color="orange" />{" "}
              {formatDate(data.release_date)}
            </p>
          </span>
          {/* <span className="flex items-center">
            <FontAwesomeIcon
              icon={faStar}
              color="orange"
              size="lg"
              className="mr-1"
            />
            <p className="text-xl font-normal">9.5</p>
          </span> */}
          <div className="flex flex-row items-center">
            <span className="text-gray-500 mr-2 text-sm">Quốc gia: </span>
            <span className="text-sm">Mỹ</span>
          </div>
          <div className="flex flex-row items-center">
            <span className="text-gray-500 mr-2 text-sm">Nhà sản xuất: </span>
            <span className="text-sm capitalize">{data.director}</span>
          </div>
          <div className="flex items-center">
            <span className="inline-block h-8 mt-2 py-[6px] text-gray-500 text-sm w-[70px] flex-0">
              Thể loại:
            </span>
            <div className="ml-2 flex flex-wrap gap-1 flex-1">
              {data.genres.map((v, i) => (
                <p
                  key={i}
                  className="capitalize border border-gray-400 text-sm rounded-md py-1 px-5"
                >
                  {v.name}
                </p>
              ))}
            </div>
          </div>
          <div className="flex items-center">
            <span className="inline-block h-8 mt-2 py-[6px] text-gray-500 text-sm w-[70px] flex-0">
              Đạo diễn:
            </span>
            <div className="ml-2 flex flex-wrap gap-1 flex-1">
              <p className="capitalize border border-gray-400 text-sm rounded-md py-1 px-5 ">
                {data.director}
              </p>
            </div>
          </div>
          <div className="flex flex-nowrap items-center">
            <span className="inline-block h-8 mt-2 py-[6px] text-gray-500 text-sm w-[70px] flex-0">
              Diễn viên:
            </span>
            <div className="ml-2 flex flex-wrap gap-1 flex-1">
              {data.actor.split(", ").map((v, i) => (
                <p
                  key={i}
                  className="capitalize border border-gray-400 text-sm rounded-md py-1 px-5 "
                >
                  {v}
                </p>
              ))}
            </div>
          </div>
        </div>
      </div>
      <div className="relative md:hidden grid grid-cols-1 grid-flow-row  lg:items-end">
        <div className=" grid grid-cols-3 gap-3 grid-flow-col">
          <div
            className={`border relative border-white col-span-1 drop-shadow-2xl z-2 ${
              !preview && "lg:-translate-y-20 md:-translate-y-16 -translate-y-0"
            }`}
          >
            <img
              width={220}
              height={280}
              src={
                isMobile ? data.image_landscape.url : data.image_portrait.url
              }
              alt={data.name}
              className='border-2 rounded border-white lg:w-[320px] lg:h-[400px] md:w-full md:h-full w-[120px] h-[160px] object-fill col-span-1  duration-500 ease-in-out group-hover:opacity-100"
      scale-100 blur-0 grayscale-0)'
            />
            {preview && (
              <div className="absolute inset-0 flex items-center justify-center z-[6]">
                <button onClick={() => setPreviewVideo(!previewVideo)}>
                  <img
                    width={64}
                    height={64}
                    src="https://www.galaxycine.vn/_next/static/media/button-play.2f9c0030.png"
                    alt=""
                  />
                </button>
              </div>
            )}
          </div>
          <div className="col-span-2 lg:-translate-y-20 flex flex-col justify-end md:-translate-y-16 -translate-y-0 gap-2">
            <h3 className="font-medium text-3xl">{data.name}</h3>
            <span className="flex gap-2">
              <p className="font-normal">
                <FontAwesomeIcon icon={faClock} color="orange" />{" "}
                {data.duration} phút
              </p>
              <p className="font-normal">
                <FontAwesomeIcon icon={faCalendar} color="orange" />{" "}
                {formatDate(data.release_date)}
              </p>
            </span>
            {/* <span className="flex items-center">
            <FontAwesomeIcon
              icon={faStar}
              color="orange"
              size="lg"
              className="mr-1"
            />
            <p className="text-xl font-normal">9.5</p>
          </span> */}
            <div className="flex flex-row items-center">
              <span className="text-gray-500 mr-2 text-sm">Quốc gia: </span>
              <span className="text-sm">Mỹ</span>
            </div>
            <div className="flex flex-row items-center">
              <span className="text-gray-500 mr-2 text-sm">Nhà sản xuất: </span>
              <span className="text-sm capitalize">{data.director}</span>
            </div>
          </div>
        </div>
        <div>
          <div className="flex flex-col gap-1 ">
            <div className="flex items-center">
              <span className="inline-block h-8 mt-2 py-[6px] text-gray-500 text-sm w-[70px] flex-0">
                Thể loại:
              </span>
              <div className="ml-2 flex flex-wrap gap-1 flex-1">
                {data.genres.map((v, i) => (
                  <p
                    key={i}
                    className="capitalize border border-gray-400 text-sm rounded-md py-1 px-5"
                  >
                    {v.name}
                  </p>
                ))}
              </div>
            </div>
            <div className="flex items-center">
              <span className="inline-block h-8 mt-2 py-[6px] text-gray-500 text-sm w-[70px] flex-0">
                Đạo diễn:
              </span>
              <div className="ml-2 flex flex-wrap gap-1 flex-1">
                <p className="capitalize border border-gray-400 text-sm rounded-md py-1 px-5 ">
                  {data.director}
                </p>
              </div>
            </div>
            <div className="flex flex-nowrap items-center">
              <span className="inline-block h-8 mt-2 py-[6px] text-gray-500 text-sm w-[70px] flex-0">
                Diễn viên:
              </span>
              <div className="ml-2 flex flex-wrap gap-1 flex-1">
                {data.actor.split(", ").map((v, i) => (
                  <p
                    key={i}
                    className="capitalize border border-gray-400 text-sm rounded-md py-1 px-5 "
                  >
                    {v}
                  </p>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default MovieCard;
