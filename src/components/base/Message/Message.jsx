import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheckCircle,
  faExclamationCircle,
  faInfoCircle,
  faExclamationTriangle
} from "@fortawesome/free-solid-svg-icons";

const MessageTypes = {
  SUCCESS: "success",
  ERROR: "error",
  INFO: "info",
  WARNING: "warning",
};

const Message = ({ content, type }) => {
  const baseClasses =
    "px-4 py-2 rounded-md shadow-lg flex items-center mb-2 animate-slide-down";
  const typeClasses = {
    [MessageTypes.SUCCESS]:
      "bg-white border-l-4 border-green-500 text-green-700",
    [MessageTypes.ERROR]: "bg-white border-l-4 border-red-500 text-red-700",
    [MessageTypes.INFO]: "bg-white border-l-4 border-blue-500 text-blue-700",
    [MessageTypes.WARNING]:
      "bg-white border-l-4 border-yellow-500 text-yellow-700",
  };

  const icons = {
    [MessageTypes.SUCCESS]: faCheckCircle,
    [MessageTypes.ERROR]: faExclamationCircle,
    [MessageTypes.INFO]: faInfoCircle,
    [MessageTypes.WARNING]: faExclamationTriangle,
  };

  return (
    <div className={`${baseClasses} ${typeClasses[type]}`}>
      <FontAwesomeIcon icon={icons[type]} className="mr-2" />
      {content}
    </div>
  );
};

const MessageContainer = () => {
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const timer = setInterval(() => {
      setMessages((prevMessages) => prevMessages.slice(1));
    }, 3000);

    return () => clearInterval(timer);
  }, []);

  const addMessage = (message) => {
    setMessages((prevMessages) => [...prevMessages, message]);
  };

  // Expose the addMessage function to the window object
  window.addMessage = addMessage;

  return (
    <div className="fixed top-5 left-1/2 transform -translate-x-1/2 z-50">
      {messages.map((msg, index) => (
        <Message key={index} {...msg} />
      ))}
    </div>
  );
};

const messageService = {
  show: (content, type = MessageTypes.INFO, duration = 3000) => {
    const message = { content, type, duration };
    const container = document.getElementById("message-container");
    if (!container) {
      const div = document.createElement("div");
      div.id = "message-container";
      document.body.appendChild(div);
      ReactDOM.render(
        <MessageContainer />,
        document.getElementById("message-container")
      );
    }
    window.addMessage(message);
  },
  success: (content, duration) =>
    messageService.show(content, MessageTypes.SUCCESS, duration),
  error: (content, duration) =>
    messageService.show(content, MessageTypes.ERROR, duration),
  info: (content, duration) =>
    messageService.show(content, MessageTypes.INFO, duration),
  warning: (content, duration) =>
    messageService.show(content, MessageTypes.WARNING, duration),
};
export default messageService;
