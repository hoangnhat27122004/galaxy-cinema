import React from "react";

const Spin = () => {
  return (
    <div className="flex items-center justify-center h-screen">
      <div className="w-16 h-16 bg-orange-500 rounded-full animate-ping animate-infinite animate-alternate-reverse animate-fill-forwards"></div>
    </div>
  );
};

export default Spin;
