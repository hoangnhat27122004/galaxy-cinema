import React from "react";

const Dialog = ({ message, onCancel }) => {
  return (
    <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[54] animate-fade">
      <div className="absolute top-0 left-0 w-full h-full bg-black opacity-50"></div>
      <div
        className="relative z-10 bg-white rounded-lg"
        style={{ padding: "30px" }}
      >
        <div className="text-center w-full">
          <img
            alt="Icon Notice"
            loading="lazy"
            width={40}
            height={40}
            decoding="async"
            data-nimg={1}
            className="w-10 h-10 inline-block"
            src="https://www.galaxycine.vn/_next/static/media/notice.e305ff4b.png"
            style={{ color: "transparent" }}
          />
          <p className="text-lg font-bold not-italic my-3">Thông báo</p>
        </div>
        <div className="  w-full">
          <p className="text-sm font-normal text-black-10 not-italic">
            {message}
          </p>
          <div className=" flex justify-center gap-3 mt-6">
            <button
              onClick={onCancel}
              type="button"
              className="rounded-lg hover:bg-orange-400 transition-all duration-30 min-w-[135px] w-full focus:outline-none text-sm text-center inline-flex items-center justify-center text-white bg-orange-500  h-10 px-5 py-2.5  capitalize font-bold "
            >
              <span className="block">Đóng</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dialog;
