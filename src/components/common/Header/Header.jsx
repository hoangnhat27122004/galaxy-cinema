import React, { useRef, useState } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRightFromBracket,
  faChevronDown,
  faBars,
} from "@fortawesome/free-solid-svg-icons";
import SignIn from "../../../pages/Auth/SignIn";
import SignUp from "../../../pages/Auth/SignUp";
import MenuComponent from "../../UI/MenuItem";
import { account, menuItems } from "./menuStructure";
import useOutsideClick from "../../../hooks/customHooks/useOutsideClick";
import useScroll from "../../../hooks/customHooks/useScroll";
import usePreviewHiddenScroll from "../../../hooks/customHooks/usePreviewHiddenScroll";
import { logout } from "../../../services/auth";
import { useAuth } from "../../../context/authContext";
import messageService from "../../base/Message/Message";
import ForgotPassword from "../../../pages/Auth/ForgotPassword";

const Header = () => {
  const [openMenu, setOpenMenu] = useState(false);
  const { isLoggedIn, setIsLoggedIn } = useAuth();
  const isScrolled = useScroll(110);
  const [openSearch, setOpenSearch] = useState(false);
  const [openForm, setOpenForm] = useState(false);
  const [isSignIn, setIsSignIn] = useState(true);
  const [isForgotPassword, setIsForgotPassword] = useState(false);
  const formRef = useRef(null);
  const [activeMenu, setActiveMenu] = useState(null);

  useOutsideClick(formRef, () => {
    if (openSearch) setOpenSearch(false);
    if (openForm) {
      setOpenForm(false);
      setIsSignIn(true);
      setIsForgotPassword(false);
    }
    if (openMenu) setOpenMenu(false);
  });

  usePreviewHiddenScroll(openForm);

  const handleLogout = async () => {
    await logout();
    setIsLoggedIn(false);
    messageService.success("Đăng xuất thành công !");
    localStorage.removeItem("customer");
    localStorage.removeItem("authToken");
  };

  return (
    <React.Fragment>
      {openForm && (
        <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center transition-all bg-black bg-opacity-45 z-[53]">
          <div
            className={`${
              !isSignIn || isForgotPassword
                ? "ssm:h-full ssm:overflow-y-auto ssm:overflow-x-hidden"
                : ""
            }`}
            ref={formRef}
          >
            <div className="bg-white max-w-[400px] ssm:min-w-[360px] min-w-[400px] rounded-md p-8 relative m-2">
              {isSignIn && !isForgotPassword ? (
                <SignIn
                  onClose={() => setOpenForm(false)}
                  onSwitch={() => setIsSignIn(false)}
                  onForgotPassword={() => setIsForgotPassword(true)}
                />
              ) : isForgotPassword ? (
                <ForgotPassword
                  onClose={() => setOpenForm(false)}
                  onSwitch={() => {
                    setIsForgotPassword(false);
                    setIsSignIn(true);
                  }}
                />
              ) : (
                <SignUp
                  onClose={() => setOpenForm(false)}
                  onSwitch={() => setIsSignIn(true)}
                />
              )}
            </div>
          </div>
        </div>
      )}
      <header
        className={`w-full z-50 bg-white ${
          isScrolled && "transition delay-300"
        }`}
      >
        <div
          style={isScrolled ? { zIndex: 52 } : {}}
          className={`${
            isScrolled
              ? "fixed w-full left-0 right-0 bg-white p-2 shadow-2xl delay-200 duration-300 ease-in-out"
              : "w-full py-4 duration-500 ease-in-out"
          }`}
        >
          <div className="my-0 mx-auto xl:max-w-[1200px] lg:max-w-[1280px] px-[16px] lg:flex flex-row justify-between items-center relative hidden">
            <Link to="/">
              <img
                width={115}
                height={60}
                src="https://www.galaxycine.vn/_next/static/media/galaxy-logo-mobile.074abeac.png"
                alt="Logo"
              />
            </Link>
            <div>
              <div className="flex justify-between items-center *:text-base *:capitalize gap-10">
                <div>
                  <Link to="/">
                    <img
                      width={112}
                      height={36}
                      src="https://www.galaxycine.vn/_next/static/media/btn-ticket.42d72c96.webp"
                      alt=""
                    />
                  </Link>
                </div>
                <MenuComponent
                  title={"phim"}
                  menuItems={menuItems}
                  activeMenu={activeMenu}
                  setActiveMenu={setActiveMenu}
                />
                <MenuComponent
                  title={"Góc điện ảnh"}
                  menuItems={menuItems}
                  activeMenu={activeMenu}
                  setActiveMenu={setActiveMenu}
                />
                <MenuComponent
                  title={"sự kiện"}
                  menuItems={menuItems}
                  activeMenu={activeMenu}
                  setActiveMenu={setActiveMenu}
                />
                <div className="hover:text-orange-400 hover:cursor-pointer">
                  Rạp/giá vé
                  <FontAwesomeIcon
                    className="ml-2"
                    icon={faChevronDown}
                    size="2xs"
                  />
                </div>
              </div>
            </div>
            <div className="flex items-center">
              {isLoggedIn ? (
                <div className="md:px-2 py-4 relative items-center text-left md:cursor-pointer group transition-all duration-500 ease-in-out lg:flex hidden">
                  <div className="w-[40px] h-[40px] leading-[62px] text-center rounded-full bg-[#D0D0D0] border-4 border-solid border-[#E9E9E2] flex-none mr-4">
                    <img
                      alt="Avatar"
                      width={40}
                      height={40}
                      className="w-full h-full rounded-full object-cover duration-500 ease-in-out group-hover:opacity-100 scale-100 blur-0 grayscale-0"
                      src="https://www.galaxycine.vn/_next/static/media/user_default.b1a2ce07.png"
                      style={{ color: "transparent" }}
                    />
                  </div>
                  <div className="flex flex-col flex-auto">
                    <div className="flex items-center justify-center gap-[6px]">
                      <img
                        alt="Logo Star Mini"
                        width={20}
                        height={30}
                        className="inline-block w-[20px] h-[30px]"
                        src="https://cdn.galaxycine.vn/media/2020/5/15/s_1589511977688.png"
                        style={{ color: "transparent" }}
                      />
                      <p className="flex-auto md:flex hidden flex-col text-sm font-bold not-italic justify-start items-start md:pr-0 group hover:text-orange-500 transition-all duration-500 ease-in-out capitalize">
                        {
                          JSON.parse(localStorage.getItem("customer"))
                            ?.full_name
                        }
                        <span className="block text-xs font-light not-italic">
                          Star
                        </span>
                      </p>
                    </div>
                    <div className="flex items-center gap-[10px]">
                      <div className="w-[20px] text-center">
                        <img
                          alt="Logo Gift"
                          width={12}
                          height={12}
                          className="inline-block w-[12px] h-[12px]"
                          src="https://www.galaxycine.vn/_next/static/media/icon-gift.190935e4.png"
                          style={{ color: "transparent" }}
                        />
                      </div>
                      <span className="flex-auto text-sm font-semibold not-italic mt-1 capitalize">
                        0 Stars
                      </span>
                    </div>
                  </div>
                  <div className="absolute left-0 w-full min-w-[150px] max-w-[220px] top-20 hidden group-hover:md:block hover:md:block z-[500] transition-all duration-500 ease-in-out">
                    <div
                      className="bg-white text-center border border-white border-solid rounded"
                      style={{
                        boxShadow:
                          "rgba(0, 0, 0, 0.08) 0px 6px 16px 0px, rgba(0, 0, 0, 0.12) 0px 3px 6px -4px, rgba(0, 0, 0, 0.05) 0px 9px 28px 8px",
                      }}
                    >
                      <ul className="flex flex-col">
                        {account.map((v, i) => (
                          <li key={i}>
                            <Link
                              to={v.path}
                              className="text-sm text-left text-black py-2 px-[18px] hover:text-[#f26b38] hover:border-l-4 hover:border-[#fd841f] hover:bg-[#fb770b1a] transition-all duration-300 flex items-center justify-between capitalize"
                            >
                              <FontAwesomeIcon icon={v.icon} />
                              <span className="grow ml-4">{v.label}</span>
                            </Link>
                          </li>
                        ))}
                        <li>
                          <button
                            onClick={handleLogout}
                            className="w-full text-sm text-left text-black py-2 px-[18px] hover:text-[#f26b38] hover:border-l-4 hover:border-[#fd841f] hover:bg-[#fb770b1a] transition-all duration-300 flex items-center justify-between capitalize"
                          >
                            <FontAwesomeIcon icon={faArrowRightFromBracket} />
                            <span className="grow ml-4">Đăng xuất</span>
                          </button>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              ) : (
                <button
                  onClick={() => setOpenForm(true)}
                  className="text-base capitalize hover:text-orange-400 pl-2"
                >
                  Đăng nhập
                </button>
              )}
            </div>
          </div>
          <div className="px-[16px] flex-row justify-between items-center relative flex lg:hidden">
            <div className="flex flex-row justify-between items-center w-full">
              <div className="flex flex-row justify-between items-center">
                <Link to="/">
                  <img
                    width={95}
                    height={40}
                    src="https://www.galaxycine.vn/_next/static/media/galaxy-logo-mobile.074abeac.png"
                    alt="Logo"
                  />
                </Link>
                <div>
                  <div className="ml-3 flex justify-between items-center *:text-base *:capitalize gap-10">
                    <div>
                      <Link to="/">
                        <img
                          width={92}
                          height={16}
                          src="https://www.galaxycine.vn/_next/static/media/btn-ticket.42d72c96.webp"
                          alt=""
                        />
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
              <button type="button" onClick={() => setOpenMenu(!openMenu)}>
                <FontAwesomeIcon
                  icon={faBars}
                  size="2xl"
                  className="text-sky-700"
                />
              </button>
            </div>
          </div>
        </div>
      </header>
      {openMenu && (
        <div className="fixed top-0 left-0 w-full h-full flex z-[54] animate-fade-right delay-0 duration-0 lg:hidden">
          <div className="absolute top-0 left-0 w-full h-full bg-black opacity-50"></div>

          <div
            ref={formRef}
            className="relative bg-white z-10 p-4"
            style={{
              width: "100%",
              maxWidth: "70vw",
              minWidth: "65%",
              height: "100vh",
            }}
          >
            {isLoggedIn ? (
              <div className="md:px-2 py-4 relative items-center text-left md:cursor-pointer group transition-all duration-500 ease-in-out  lg:hidden">
                <div
                  className="flex items-center w-full"
                  onClick={() =>
                    setActiveMenu(activeMenu === "account" ? null : "account")
                  }
                >
                  <div className="w-[40px] h-[40px] leading-[62px] text-center rounded-full bg-[#D0D0D0] border-4 border-solid border-[#E9E9E2] flex-none mr-4">
                    <img
                      alt="Avatar"
                      width={40}
                      height={40}
                      className="w-full h-full rounded-full object-cover duration-500 ease-in-out group-hover:opacity-100 scale-100 blur-0 grayscale-0"
                      src="https://www.galaxycine.vn/_next/static/media/user_default.b1a2ce07.png"
                      style={{ color: "transparent" }}
                    />
                  </div>
                  <div className="flex flex-col flex-auto">
                    <div className="flex items-center justify-center gap-[6px]">
                      <img
                        alt="Logo Star Mini"
                        width={20}
                        height={30}
                        className="inline-block w-[20px] h-[30px]"
                        src="https://cdn.galaxycine.vn/media/2020/5/15/s_1589511977688.png"
                        style={{ color: "transparent" }}
                      />
                      <p className="flex-auto flex lg:hidden flex-col text-sm font-bold not-italic justify-start items-start md:pr-0 group hover:text-orange-500 transition-all duration-500 ease-in-out capitalize">
                        {
                          JSON.parse(localStorage.getItem("customer"))
                            ?.full_name
                        }
                        <span className="block text-xs font-light not-italic">
                          Star
                        </span>
                      </p>
                    </div>
                    <div className="flex items-center gap-[10px]">
                      <div className="w-[20px] text-center">
                        <img
                          alt="Logo Gift"
                          width={12}
                          height={12}
                          className="inline-block w-[12px] h-[12px]"
                          src="https://www.galaxycine.vn/_next/static/media/icon-gift.190935e4.png"
                          style={{ color: "transparent" }}
                        />
                      </div>
                      <span className="flex-auto text-sm font-semibold not-italic mt-1 capitalize">
                        0 Stars
                      </span>
                    </div>
                  </div>
                </div>
                <div
                  className={`w-full ${
                    activeMenu === "account" ? "block" : "hidden"
                  } mt-4`}
                >
                  <div
                    className="bg-white text-center border border-white border-solid rounded"
                    style={{
                      boxShadow:
                        "rgba(0, 0, 0, 0.08) 0px 6px 16px 0px, rgba(0, 0, 0, 0.12) 0px 3px 6px -4px, rgba(0, 0, 0, 0.05) 0px 9px 28px 8px",
                    }}
                  >
                    <ul className="flex flex-col">
                      {account.map((v, i) => (
                        <li key={i}>
                          <Link
                            to={v.path}
                            className="text-sm text-left text-black py-2 px-[18px] hover:text-[#f26b38] hover:border-l-4 hover:border-[#fd841f] hover:bg-[#fb770b1a] transition-all duration-300 flex items-center justify-between capitalize"
                          >
                            <FontAwesomeIcon icon={v.icon} />
                            <span className="grow ml-4">{v.label}</span>
                          </Link>
                        </li>
                      ))}
                      <li>
                        <button
                          onClick={handleLogout}
                          className="w-full text-sm text-left text-black py-2 px-[18px] hover:text-[#f26b38] hover:border-l-4 hover:border-[#fd841f] hover:bg-[#fb770b1a] transition-all duration-300 flex items-center justify-between capitalize"
                        >
                          <FontAwesomeIcon icon={faArrowRightFromBracket} />
                          <span className="grow ml-4">Đăng xuất</span>
                        </button>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            ) : (
              <button
                onClick={() => {
                  setOpenMenu(false);
                  setOpenForm(true);
                }}
                className="text-base capitalize hover:text-orange-400 pl-2"
              >
                Đăng nhập
              </button>
            )}
            <MenuComponent
              title={"phim"}
              menuItems={menuItems}
              activeMenu={activeMenu}
              setActiveMenu={setActiveMenu}
            />
            <MenuComponent
              title={"Góc điện ảnh"}
              menuItems={menuItems}
              activeMenu={activeMenu}
              setActiveMenu={setActiveMenu}
            />
            <MenuComponent
              title={"sự kiện"}
              menuItems={menuItems}
              activeMenu={activeMenu}
              setActiveMenu={setActiveMenu}
            />
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

export default Header;
