import {
  faArrowRightFromBracket,
  faList,
  faUserNinja,
} from "@fortawesome/free-solid-svg-icons";
export const menuItems = [
  { label: "Thể Loại Phim", path: "/the-loai" },
  { label: "Diễn Viên", path: "/dien-vien" },
  { label: "Đạo Diễn", path: "/dao-dien" },
  { label: "Bình Luận Phim", path: "/binh-luan" },
  { label: "Blog Điện Ảnh", path: "/blog" },
];

export const account = [
  { label: "Tài khoản", icon: faUserNinja, path: "/tai-khoan#profile" },
  { label: "Lịch sử", icon: faList, path: "/tai-khoan#transaction" },
];
