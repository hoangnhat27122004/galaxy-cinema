import { useQuery } from "@tanstack/react-query";
import { getSchedulesByMovieId } from "../../services/schedule";
const useScheduleQuery = (id) => {
  const queryKey = ["SCHEDULE_KEY", id];

  const { data, ...rest } = useQuery({
    queryKey,
    queryFn: async () => {
      return await getSchedulesByMovieId(id);
    },
    staleTime: 60000,
    cacheTime: 300000,
  });

  return { data, ...rest };
};

export default useScheduleQuery;
