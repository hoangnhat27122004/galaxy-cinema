import { useState, useEffect } from "react";

const useAnimation = (initialAnimation, onClose) => {
  const [animate, setAnimate] = useState(initialAnimation);

  useEffect(() => {
    const handleAnimationEnd = () => {
      if (animate === "animate-jump-out") {
        onClose();
      }
    };

  }, [animate, onClose]);

  const triggerJumpOut = () => {
    setAnimate("animate-jump-out");
  };

  return [animate, triggerJumpOut];
};

export default useAnimation;
