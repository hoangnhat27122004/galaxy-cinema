import Joi from "joi";
const minBirthYear = 2011;
export const signUpValidator = Joi.object({
  full_name: Joi.string().required().min(2).max(255).messages({
    "string.base": `Vui lòng nhập họ tên của bạn.`,
    "string.empty": `Vui lòng nhập họ tên của bạn.`,
    "string.min": `Họ tên phải có độ dài tối thiểu là {#limit} ký tự`,
    "string.max": `Họ tên phải có độ dài tối đa là {#limit} ký tự`,
    "any.required": `Vui lòng nhập họ tên của bạn.`,
  }),
  birthday: Joi.alternatives()
    .try(
      Joi.date().max("2011-12-31").raw(),
      Joi.string().custom((value, helpers) => {
        if (typeof value !== "string") {
          return helpers.error("date.format");
        }
        const [day, month, year] = value.split("-");
        const date = new Date(year, month - 1, day);
        if (isNaN(date.getTime())) {
          return helpers.error("date.format");
        }
        if (date > new Date("2011-12-31")) {
          return helpers.error("date.max");
        }
        return date;
      }, "date format validation")
    )
    .required()
    .messages({
      "date.base": "Vui lòng chọn ngày tháng năm sinh của bạn",
      "date.max": "Ngày sinh phải trước hoặc là ngày 31-12-2011",
      "any.required": "Ngày sinh là bắt buộc",
      "date.format": "Định dạng ngày không hợp lệ",
      "alternatives.types": "Vui lòng chọn ngày tháng năm sinh của bạn",
    }),
  gender: Joi.string().valid("male", "female", "unknown").required().messages({
    "any.only": `Vui lòng chọn giới tính của bạn`,
    "any.required": `Vui lòng chọn giới tính của bạn.`,
  }),
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .required()
    .min(2)
    .max(255)
    .messages({
      "string.base": `Vui lòng nhập email của bạn.`,
      "string.empty": `Vui lòng nhập email của bạn.`,
      "string.email": `Định dạng email không hợp lệ`,
      "string.min": `Email phải có độ dài tối thiểu là {#limit} ký tự`,
      "string.max": `Email phải có độ dài tối đa là {#limit} ký tự`,
      "any.required": `Vui lòng nhập email của bạn.`,
    }),
  phone: Joi.string()
    .required()
    .min(10)
    .pattern(/^[0-9]+$/)
    .messages({
      "string.base": `Vui lòng nhập số điện thoại của bạn.`,
      "string.empty": `Vui lòng nhập số điện thoại của bạn.`,
      "string.pattern.base": `Sai định dạng số điện thoại`,
      "string.min": `Số điện thoại phải có ít nhất {#limit} chữ số`,
      "any.required": `Vui lòng nhập số điện thoại của bạn.`,
    }),
  password: Joi.string().required().min(6).max(255).messages({
    "string.base": `Vui lòng nhập mật khẩu của bạn.`,
    "string.empty": `Vui lòng nhập mật khẩu của bạn.`,
    "string.min": `Mật khẩu phải có độ dài tối thiểu là {#limit} ký tự`,
    "string.max": `Mật khẩu phải có độ dài tối đa là {#limit} ký tự`,
    "any.required": `Vui lòng nhập mật khẩu của bạn.`,
  }),
});

export const signInValidator = Joi.object({
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .required()
    .min(2)
    .max(255)
    .messages({
      "string.base": `Vui lòng nhập email của bạn.`,
      "string.empty": `Vui lòng nhập email của bạn.`,
      "string.email": `Định dạng email không hợp lệ`,
      "string.min": `Email phải có độ dài tối thiểu là {#limit} ký tự`,
      "string.max": `Email phải có độ dài tối đa là {#limit} ký tự`,
      "any.required": `Vui lòng nhập email của bạn.`,
    }),
  password: Joi.string().required().min(6).max(255).messages({
    "string.base": `Vui lòng nhập mật khẩu.`,
    "string.empty": `Vui lòng nhập mật khẩu.`,
    "string.min": `Mật khẩu phải có độ dài tối thiểu là {#limit} ký tự`,
    "string.max": `Mật khẩu phải có độ dài tối đa là {#limit} ký tự`,
    "any.required": `Vui lòng nhập mật khẩu.`,
  }),
});

export const bookingWithoutLoginValidator = Joi.object({
  phone: Joi.string().required().min(10).max(10).messages({
    "string.base": `Vui lòng nhập số điện thoại của bạn.`,
    "string.empty": `Vui lòng nhập số điện thoại của bạn.`,
    "string.min": `Số điện thoại phải có độ dài tối thiểu là {#limit} ký tự`,
    "string.max": `Số điện thoại phải có độ dài tối đa là {#limit} ký tự`,
    "any.required": `Vui lòng nhập số điện thoại của bạn.`,
  }),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: false } })
    .required()
    .min(2)
    .max(255)
    .messages({
      "string.base": `Vui lòng nhập email của bạn.`,
      "string.empty": `Vui lòng nhập email của bạn.`,
      "string.email": `Định dạng email không hợp lệ`,
      "string.min": `Email phải có độ dài tối thiểu là {#limit} ký tự`,
      "string.max": `Email phải có độ dài tối đa là {#limit} ký tự`,
      "any.required": `Vui lòng nhập email của bạn.`,
    }),
});

export const changePasswordValidate = Joi.object({
  oldPassword: Joi.string().required().min(6).max(255).messages({
    "string.base": `Vui lòng nhập mật khẩu cũ.`,
    "string.empty": `Vui lòng nhập mật khẩu cũ.`,
    "string.min": `Mật khẩu cũ phải có độ dài tối thiểu là {#limit} ký tự`,
    "string.max": `Mật khẩu cũ phải có độ dài tối đa là {#limit} ký tự`,
    "any.required": `Vui lòng nhập mật khẩu cũ.`,
  }),
  newPassword: Joi.string().required().min(6).max(255).messages({
    "string.base": `Vui lòng nhập mật khẩu mới.`,
    "string.empty": `Vui lòng nhập mật khẩu mới.`,
    "string.min": `Mật khẩu mới phải có độ dài tối thiểu là {#limit} ký tự`,
    "string.max": `Mật khẩu mới phải có độ dài tối đa là {#limit} ký tự`,
    "any.required": `Vui lòng nhập mật khẩu mới.`,
  }),
});

export const forgotPasswordValidate = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: false } })
    .required()
    .min(2)
    .max(255)
    .messages({
      "string.base": `Vui lòng nhập email của bạn.`,
      "string.empty": `Vui lòng nhập email của bạn.`,
      "string.email": `Định dạng email không hợp lệ`,
      "string.min": `Email phải có độ dài tối thiểu là {#limit} ký tự`,
      "string.max": `Email phải có độ dài tối đa là {#limit} ký tự`,
      "any.required": `Vui lòng nhập email của bạn.`,
    }),
});
