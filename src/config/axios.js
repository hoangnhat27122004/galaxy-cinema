import axios from "axios";
import { jwtDecode } from "jwt-decode";

const axiosAuthInstance = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  withCredentials: true,
});

const axiosNoAuthInstance = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  withCredentials: true,
});

let isRefreshing = false;
let refreshSubscribers = [];

const refreshToken = async () => {
  try {
    const response = await axiosNoAuthInstance.post("/refresh-token");
    return response.data;
  } catch (error) {
    throw error;
  }
};

const onRefreshed = (token) => {
  refreshSubscribers.forEach((callback) => callback(token));
  refreshSubscribers = [];
};

const addRefreshSubscriber = (callback) => {
  refreshSubscribers.push(callback);
};

axiosAuthInstance.interceptors.request.use(
  async (config) => {
    const token = localStorage.getItem("authToken");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosAuthInstance.interceptors.response.use(
  (response) => response,
  async (error) => {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {
      if (isRefreshing) {
        return new Promise((resolve) => {
          addRefreshSubscriber((token) => {
            originalRequest.headers.Authorization = `Bearer ${token}`;
            resolve(axiosAuthInstance(originalRequest));
          });
        });
      }

      originalRequest._retry = true;
      isRefreshing = true;

      try {
        const newToken = await refreshToken();
        localStorage.setItem("authToken", newToken.accessToken);
        axiosAuthInstance.defaults.headers.common[
          "Authorization"
        ] = `Bearer ${newToken.accessToken}`;
        onRefreshed(newToken.accessToken);
        originalRequest.headers.Authorization = `Bearer ${newToken.accessToken}`;
        return axiosAuthInstance(originalRequest);
      } catch (refreshError) {
        localStorage.removeItem("authToken");
        localStorage.removeItem("customer");
        return Promise.reject(refreshError);
      } finally {
        isRefreshing = false;
      }
    }
    return Promise.reject(error);
  }
);

export const apiAuth = axiosAuthInstance;
export const apiNoAuth = axiosNoAuthInstance;
